﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

[RequireComponent(typeof(CircleCollider2D))]
public class NoisyRadarScan : RadarScan
{
    public override bool CanBeHeard()
    {
        return true;
    }
}
