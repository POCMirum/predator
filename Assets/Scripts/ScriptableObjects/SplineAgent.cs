﻿using UnityEngine;

[System.Serializable]
public class SplineAgent
{
    public Transform transform;
    public float distanceFromStart;
    public bool forward = true;

    //private PlatformController platformController;
    private bool CheckedPlatformController;

    public void UpdatePosition(Vector3 position)
    {
        if (!CheckedPlatformController)
        {
            //platformController = transform.gameObject.GetComponent<PlatformController>();
            CheckedPlatformController = true;
        }

        //if (platformController != null)
        //{
        //    platformController.UpdateCustom(position);
        //}
        else
        {
            transform.position = position;
        }
    }

    public void ReverseOrientation()
    {
        Vector3 temp = transform.localScale;
        temp.x *= -1f;
        transform.localScale = temp;
    }
}
