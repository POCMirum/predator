﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using ProGrids;

[CustomEditor(typeof(SplinePath))]
public class SplinePathEditor : Editor
{
    private SplinePath current;
    static Texture2D texDotPlus;
    static Texture2D texDot;
    static Texture2D texMinus;

    private void CapDotPlus(int aControlID, Vector3 aPosition, Quaternion aRotation, float aSize, EventType aType) { Ferr.EditorTools.ImageCapBase(aControlID, aPosition, aRotation, aSize, texDotPlus, aType); }
    private void CapDot(int aControlID, Vector3 aPosition, Quaternion aRotation, float aSize, EventType aType) { Ferr.EditorTools.ImageCapBase(aControlID, aPosition, aRotation, aSize, texDot, aType); }
    private void CapDotMinus(int aControlID, Vector3 aPosition, Quaternion aRotation, float aSize, EventType aType) { Ferr.EditorTools.ImageCapBase(aControlID, aPosition, aRotation, aSize, texMinus, aType); }

    private void OnEnable()
    {
        current = (SplinePath)target;
        current.snapValue = pg_Editor.SnapValue();
        current.Initialize();

        if (texDot == null)
        {
            texDotPlus = Ferr.EditorTools.GetGizmo("2D/Gizmos/dot-plus.png");
            texDot = Ferr.EditorTools.GetGizmo("2D/Gizmos/dot.png");
            texMinus = Ferr.EditorTools.GetGizmo("2D/Gizmos/dot-minus.png");
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }

    protected virtual void OnSceneGUI()
    {
        if (current?.initialNodes == null && current.initialNodes.Count < 2)
        {
            return;
        }

        for (int i = 0; i < current.initialNodes.Count; i++)
        {
            if (Event.current.alt)
            {
                float handleScale = Ferr2D_PathEditor.HandleScale(current.initialNodes[i].position) * Ferr2D_Visual.HandleSize;
                if (Handles.Button(current.initialNodes[i].position, Quaternion.identity, handleScale, handleScale, CapDotMinus))
                {
                    current.RemoveNode(i);
                    GUI.changed = true;
                }
            }
            else
            {
                float handleScale = Ferr2D_PathEditor.HandleScale(current.initialNodes[i].position) * Ferr2D_Visual.HandleSize;

                current.initialNodes[i].position = pg_Util.SnapValue(current.initialNodes[i].position, pg_Editor.SnapValue());

                Vector3 position = Handles.FreeMoveHandle(current.initialNodes[i].position, Quaternion.identity, handleScale * Ferr2D_Visual.HandleSize, Vector3.zero, CapDot);
                current.initialNodes[i].position = pg_Util.SnapValue(position, pg_Editor.SnapValue());

                //Vector3 position = Handles.PositionHandle(current.initialNodes[i].position, Quaternion.identity);

                if (i == 0) continue;

                Vector3 mid = (current.initialNodes[i - 1].position + current.initialNodes[i].position) / 2;

                handleScale = Ferr2D_PathEditor.HandleScale(mid) * Ferr2D_Visual.HandleSize;

                if (Handles.Button(mid, Quaternion.identity, handleScale, handleScale, CapDotPlus))
                {
                    current.InsertNode(i, mid);
                    GUI.changed = true;
                }
            }
        }
    }
}