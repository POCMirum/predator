﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplinePath : MonoBehaviour
{
    [Header("Spline Path")]
    public List<Transform> initialNodes;
    public int curveResolution;
    public bool loop;

    [HideInInspector]
    public float snapValue;

    protected List<Vector3> splinePoints = new List<Vector3>();

    public void Initialize()
    {
        if (initialNodes == null)
        {
            initialNodes = new List<Transform>();
        }

        if (initialNodes.Count == 0)
        {
            InsertNode(0, transform.position);
            InsertNode(1, transform.position + Vector3.right * 4f);
        }
    }

    public void InsertNode(int index, Vector3 position)
    {
        Transform node = (new GameObject("node")).transform;
        node.parent = this.transform;
        node.position = position;

        if (index >= initialNodes.Count)
        {
            initialNodes.Add(node);
        }
        else if (index >= 0)
        {
            initialNodes.Insert(index, node);
        }
    }

    public void RemoveNode(int index)
    {
        if (index >= 0 && index < initialNodes.Count && initialNodes.Count - 1 > 1)
        {
            DestroyImmediate(initialNodes[index].gameObject);
            initialNodes.RemoveAt(index);
        }
    }

    protected float GetTotalDistance()
    {
        splinePoints.Clear();
        splinePoints.AddRange(Interpolate.NewCatmullRom(initialNodes, curveResolution, loop));

        return GetTotalDistance(splinePoints);
    }

    protected float GetTotalDistance(List<Vector3> points)
    {
        float totalDistance = 0f;
        int count = points.Count;
        for (int i = 1; i < count; ++i)
        {
            totalDistance += (points[i] - points[i - 1]).magnitude;
        }

        return totalDistance;
    }

    public List<Vector3> GetPlacementPosition(int amount)
    {
        List<Vector3> result = new List<Vector3>(amount);

        if (amount == 0) return result;

        splinePoints.Clear();
        splinePoints.AddRange(Interpolate.NewCatmullRom(initialNodes, curveResolution, loop));

        float totalDistance = GetTotalDistance(splinePoints);
        float distanceBetweenObjects = totalDistance / amount;
        float distanceToMove = distanceBetweenObjects * 0.5f;

        if (amount > 2 && !loop)
        {
            result.Add(splinePoints[0]);
            result.Add(splinePoints[splinePoints.Count - 1]);

            distanceBetweenObjects = totalDistance / (amount - 1);
            distanceToMove = distanceBetweenObjects;
        }

        int nextSplinePointIndex = 1;
        Vector3 positionIterator = splinePoints[0];

        while (nextSplinePointIndex < splinePoints.Count)
        {
            Vector3 direction = (splinePoints[nextSplinePointIndex] - positionIterator);
            float distanceToNextPoint = direction.magnitude;
            direction = direction.normalized;
            if (distanceToNextPoint >= distanceToMove)
            {
                positionIterator += direction * distanceToMove;

                result.Add(positionIterator);
                distanceToMove = distanceBetweenObjects;
            }
            else
            {
                distanceToMove -= distanceToNextPoint;
                positionIterator = splinePoints[nextSplinePointIndex++];
            }
        }

        return result;
    }

    public Vector3 GetPosition(float distanceFromStart)
    {
        splinePoints.Clear();
        splinePoints.AddRange(Interpolate.NewCatmullRom(initialNodes, curveResolution, loop));

        int nextSplinePointIndex = 1;
        Vector3 positionIterator = splinePoints[0];
        float totalDistance = GetTotalDistance(splinePoints);

        float distanceToMove = distanceFromStart;

        if (distanceFromStart < 0)
        {
            distanceToMove = distanceFromStart % -totalDistance;
            distanceToMove = totalDistance - distanceToMove;
        }
        else if (distanceFromStart > totalDistance)
        {
            distanceToMove = distanceFromStart % totalDistance;
        }

        while (nextSplinePointIndex < splinePoints.Count)
        {
            Vector3 direction = (splinePoints[nextSplinePointIndex] - positionIterator);
            float distanceToNextPoint = direction.magnitude;
            direction.Normalize();
            if (distanceToNextPoint >= distanceToMove)
            {
                positionIterator += direction * distanceToMove;

                return positionIterator;
            }
            else
            {
                distanceToMove -= distanceToNextPoint;
                positionIterator = splinePoints[nextSplinePointIndex++];
            }

            if (nextSplinePointIndex >= splinePoints.Count && loop)
            {
                nextSplinePointIndex = 0;
            }
        }

        return positionIterator;
    }

    private void OnDrawGizmos()
    {
        if (initialNodes == null) return;
        if (initialNodes.Count < 2) return;

        for (int i = 0; i < initialNodes.Count; i++)
        {
            if (snapValue != 0)
            {
                initialNodes[i].transform.position = ProGrids.pg_Util.SnapValue(initialNodes[i].transform.position, snapValue);
            }
            Gizmos.DrawWireSphere(initialNodes[i].transform.position, 0.15f);
        }

        splinePoints.Clear();
        splinePoints.AddRange(Interpolate.NewCatmullRom(initialNodes, curveResolution, loop));

        for (int i = 1; i < splinePoints.Count; ++i)
        {
            Gizmos.DrawLine(splinePoints[i - 1], splinePoints[i]);
        }
    }
}
