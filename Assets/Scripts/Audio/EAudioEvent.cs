﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2017 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System;
using System.Collections.Generic;


public delegate void RandomSFXDelegate(List<string> pArg);

public delegate void PlaySFXVolumeDelegate(string pSFX, float pVolumePercent);

public delegate void PlayRandomSFXVolumeDelegate(List<string> pSFX, float pVolumePercent);

public delegate void MusicTransitionDelegate(string pName, bool pLoop, bool pFade, bool pAmbiant);

public delegate void ManageableSFXDelegate(string pSFXToPlay, GameObject pCreatorGameObject);

public delegate void ManageableSFXVolumeDelegate(string pSFXToPlay, GameObject pCreatorGameObject, float pVolumePercent);
[RadixEventEnum]
public enum EAudioEvent
{
    [EventHandlerAttribute(typeof(MusicTransitionDelegate))]
    MUSIC_TRANSITION,

    [EventHandlerAttribute(typeof(StringDelegate))]
    PLAY_SFX,

    [EventHandlerAttribute(typeof(RandomSFXDelegate))]
    PLAY_RANDOM_SFX,

    [EventHandlerAttribute(typeof(PlaySFXVolumeDelegate))]
    PLAY_SFX_VOLUME,

    [EventHandlerAttribute(typeof(PlayRandomSFXVolumeDelegate))]
    PLAY_RANDOM_SFX_VOLUME,

    [EventHandlerAttribute()]
    SETTING_CHANGED,

    [EventHandlerAttribute()]
    STOP_MUSIC,

    [EventHandlerAttribute()]
    PLAY_MUSIC,

    [EventHandlerAttribute()]
    SFX_MANAGER_READY,

    [EventHandlerAttribute(typeof(ManageableSFXDelegate))]
    PLAY_MANAGEABLE_SFX,

    [EventHandlerAttribute(typeof(ManageableSFXDelegate))]
    PLAY_SFX_CHAIN_START,

    [EventHandlerAttribute(typeof(ManageableSFXDelegate))]
    PLAY_SFX_CHAIN_END,

    [EventHandlerAttribute(typeof(ManageableSFXDelegate))]
    PLAY_SFX_LOOP,

    [EventHandlerAttribute(typeof(ManageableSFXDelegate))]
    STOP_SFX,

    [EventHandlerAttribute(typeof(ManageableSFXVolumeDelegate))]
    PLAY_MANAGEABLE_SFX_VOLUME,

    [EventHandlerAttribute(typeof(ManageableSFXVolumeDelegate))]
    PLAY_SFX_VOLUME_LOOP
}