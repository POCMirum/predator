﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

public class ManageableSFX {

    private AudioClip mAudioClip;

    private GameObject mCreatorGameObject;

    //This will be used if a gameobject uses different SFX that do not always need to be stopped at the same time
    private string mName = "";

    private int mAudioSourceIndex = -1;
    private bool mAudioSourceIsSet = false;
    private bool mHasAChain = false;

    private bool mShouldLoop = false;

    public ManageableSFX (AudioClip pAudioClip, GameObject pCreatorGameObject, string pName) {
        mAudioClip = pAudioClip;
        mCreatorGameObject = pCreatorGameObject;
        mName = pName;
    }

    public AudioClip GetAudioClip()
    {
        return mAudioClip;
    }

    public GameObject GetCreatorGameObject()
    {
        return mCreatorGameObject;
    }

    public string GetName()
    {
        return mName;
    }

    public void SetAudioSourceIndex(int pAudioSourceIndex)
    {
        mAudioSourceIndex = pAudioSourceIndex;
        mAudioSourceIsSet = true;
    }

    public int GetAudioSourceIndex()
    {
        return mAudioSourceIndex;
    }

    public void SetHasAChain(bool pHasAChain)
    {
        mHasAChain = pHasAChain;
    }

    public bool HasAChain()
    {
        return mHasAChain;
    }

    public void SetShouldLoop(bool pShouldLoop)
    {
        mShouldLoop = pShouldLoop;
    }

    public bool ShouldLoop()
    {
        return mShouldLoop;
    }

    public bool AudioSourceIsSet()
    {
        return mAudioSourceIsSet;
    }
}