﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

public class AudioArtistControl : RadixMonoBehaviour {

    [SerializeField]
    private AudioClip m_AudioClip;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float m_Volume = 1.0f;
    [SerializeField]
    private bool m_ShouldLoop = false;

    //TODO implement interface for randomized SFX
    //TODO implement pitch, stereo pan, spatial blend and reverb in this and the SFX manager

    public void PlaySFX()
    {
        //TODO we could check whether or not the settings require a manageable sfx. If they don't, send a PLAY_SFX_VOLUME event instead, to save memory
        PlayManageableSFX ();
    }

    public void StopSFX()
    {
        DispatchEvent (EAudioEvent.STOP_SFX, m_AudioClip.name, this.gameObject);
    }

    private void PlayManageableSFX()
    {
        if (m_AudioClip != null) {
            if (m_ShouldLoop) {
                DispatchEvent (EAudioEvent.PLAY_SFX_VOLUME_LOOP, m_AudioClip.name, this.gameObject, m_Volume);
            } else {
                DispatchEvent (EAudioEvent.PLAY_MANAGEABLE_SFX_VOLUME, m_AudioClip.name, this.gameObject, m_Volume);
            }
        }
    }

    [ContextMenu ("Play SFX Now")]
    void PlaySFXNow () {
        if (Application.isPlaying) {
            PlaySFX ();
        }
    }
}