﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

public class SFXManager : RadixMonoBehaviour {

    public const string RESOURCES_SFX_PREFIX = "SFX/";

    private const int MAX_SIMULTANEOUS_SFX = 10;

    private AudioSource[] mAudioSources = new AudioSource[MAX_SIMULTANEOUS_SFX];

    private List<ManageableSFX> mManageableSFXList = new List<ManageableSFX>();

    public static bool IsInit = false;

    private void Start () {
        GetInternalAudioSources ();
        if(GetComponent<IndestructibleObject>() != null)
        {
            IsInit = true;
        }
        else
        {   
            Error.Create("The SFX manager isn't indestructible");
        }
        RegisterEvent<StringDelegate> (EAudioEvent.PLAY_SFX, OnPlaySFX);
        RegisterEvent<RandomSFXDelegate> (EAudioEvent.PLAY_RANDOM_SFX, OnPlayRandomSFX);
        RegisterEvent<PlaySFXVolumeDelegate> (EAudioEvent.PLAY_SFX_VOLUME, OnPlaySFXVolume);
        RegisterEvent<PlayRandomSFXVolumeDelegate> (EAudioEvent.PLAY_RANDOM_SFX_VOLUME, OnPlayRandomSFXVolume);
        RegisterEvent<ManageableSFXDelegate> (EAudioEvent.PLAY_MANAGEABLE_SFX, PlayManageableSFX);
        RegisterEvent<ManageableSFXDelegate> (EAudioEvent.PLAY_SFX_CHAIN_START, PlayChainSFX);
        RegisterEvent<ManageableSFXDelegate> (EAudioEvent.PLAY_SFX_LOOP, PlayLoopSFX);
        RegisterEvent<ManageableSFXDelegate> (EAudioEvent.STOP_SFX, OnStopSFX);
        RegisterEvent<ManageableSFXVolumeDelegate> (EAudioEvent.PLAY_MANAGEABLE_SFX_VOLUME, PlayManageableSFXVolume);
        RegisterEvent<ManageableSFXVolumeDelegate> (EAudioEvent.PLAY_SFX_VOLUME_LOOP, PlayLoopSFXVolume);
    }

    private void Update()
    {
        CleanManageableSFXIndexes ();
    }

    private void GetInternalAudioSources()
    {
        mAudioSources = GetComponents<AudioSource>();

        Assert.CheckNull(mAudioSources, "The Audio System doesn't have an audio source in its components");
    }

    private void OnPlaySFX(string pSFXToPlay)
    {
        PlaySFX (pSFXToPlay);
    }

    private void OnPlayRandomSFX(List<string> pAudioSources)
    {
        string sfxToPlay = null;
        sfxToPlay = pAudioSources [Random.Range (0, pAudioSources.Count)];
        PlaySFX (sfxToPlay);
    }

    //if pVolumePercent equals 1, the SFX's volume will be maxed
    private void OnPlaySFXVolume(string pSFXToPlay, float pVolumePercent)
    {
        PlaySFXVolume (pSFXToPlay, pVolumePercent);
    }

    private void PlaySFX(string pSFXToPlay)
    {
        PlaySFXVolume (pSFXToPlay, 1);
    }

    private void PlaySFXVolume(string pSFXToPlay, float pVolumePercent)
    {
        if (!string.IsNullOrEmpty (pSFXToPlay)) {
            for (int i = 0; i < mAudioSources.Length; i++) {
                if (!mAudioSources [i].isPlaying) {
                    AudioClip audioClip = Resources.Load<AudioClip> (RESOURCES_SFX_PREFIX + pSFXToPlay);
                    mAudioSources [i].volume = pVolumePercent; // * OptionService.Get.SFXVolume;
                    mAudioSources [i].clip = audioClip;
                    mAudioSources [i].Play ();
                    break;
                }
            }
        }
    }

    private void PlayManageableSFX(ManageableSFX pSFXToPlay)
    {
        PlayManageableSFXVolume (pSFXToPlay, 1);
    }

    private void PlayManageableSFX(ManageableSFX pSFXToPlay, float pVolume)
    {
        PlayManageableSFXVolume (pSFXToPlay, pVolume);
    }

    private void PlayManageableSFXVolume(ManageableSFX pSFXToPlay, float pVolumePercent)
    {
        for (int i = 0; i < mAudioSources.Length; i++) {
            if (!mAudioSources [i].isPlaying) {
                pSFXToPlay.SetAudioSourceIndex (i);
                AudioClip audioClip = pSFXToPlay.GetAudioClip();
                mAudioSources [i].volume = pVolumePercent; //* OptionService.Get.SFXVolume;
                mAudioSources [i].clip = audioClip;
                mAudioSources [i].loop = pSFXToPlay.ShouldLoop ();
                mAudioSources [i].Play ();
                break;
            }
        }
    }

    private void OnPlayRandomSFXVolume(List<string> pAudioSources, float pVolumePercent)
    {
        string sfxToPlay = null;
        sfxToPlay = pAudioSources [Random.Range (0, pAudioSources.Count)];
        PlaySFXVolume (sfxToPlay, pVolumePercent);
    }

    private void CleanManageableSFXIndexes()
    {
        //This function makes sure that manageableSFX are forgotten when they stop playing
        if (mManageableSFXList.Count > 0) {
            List<int> indexesToRemove = new List<int> ();
            for (int i = 0; i < mManageableSFXList.Count; i++) {
                ManageableSFX sfx = mManageableSFXList [i];

                if (!sfx.AudioSourceIsSet ()) {
                    PlayManageableSFXVolume (mManageableSFXList [i], 1);
                } else if (!mAudioSources [sfx.GetAudioSourceIndex ()].isPlaying) {
                    indexesToRemove.Add (i);
                }
            }
            for (int i = 0; i < indexesToRemove.Count; i++) {
                ManageableSFX sfxToRemove = mManageableSFXList [indexesToRemove [i]];
                if (sfxToRemove.HasAChain ()) {
                    DispatchEvent (EAudioEvent.PLAY_SFX_CHAIN_END, sfxToRemove.GetName (), sfxToRemove.GetCreatorGameObject ());
                }
                mAudioSources [sfxToRemove.GetAudioSourceIndex ()].loop = false;
                mManageableSFXList.RemoveAt (indexesToRemove [i]);
            }
        }
    }

    private void PlayManageableSFX(string pSFXToPlay, GameObject pCreatorGameObject)
    {
        ManageableSFX newManageableSFX = CreateManageableSFX (pSFXToPlay, pCreatorGameObject);
        PlayManageableSFX (newManageableSFX);
    }

    private void PlayChainSFX(string pSFXToPlay, GameObject pCreatorGameObject)
    {
        ManageableSFX newManageableSFX = CreateManageableSFX (pSFXToPlay, pCreatorGameObject);
        newManageableSFX.SetHasAChain (true);
        PlayManageableSFX (newManageableSFX);
    }

    private void PlayLoopSFX(string pSFXToPlay, GameObject pCreatorGameObject)
    {
        ManageableSFX newManageableSFX = CreateManageableSFX (pSFXToPlay, pCreatorGameObject);
        newManageableSFX.SetShouldLoop (true);
        PlayManageableSFX (newManageableSFX);
    }

    private ManageableSFX CreateManageableSFX(string pSFXToPlay, GameObject pCreatorGameObject)
    {
        AudioClip newAudioClip = Resources.Load<AudioClip>(RESOURCES_SFX_PREFIX + pSFXToPlay);
        ManageableSFX newManageableSFX = new ManageableSFX(newAudioClip, pCreatorGameObject, pSFXToPlay);
        mManageableSFXList.Add (newManageableSFX);
        return newManageableSFX;
    }

    private void OnStopSFX(string pSFXToStop, GameObject pCreatorGameObject)
    {
        for (int i = 0; i < mManageableSFXList.Count; i++) {
            if (mManageableSFXList [i].GetName ().Equals (pSFXToStop) && mManageableSFXList [i].GetCreatorGameObject() == pCreatorGameObject) {
                if (mManageableSFXList [i].HasAChain ()) {
                    DispatchEvent (EAudioEvent.PLAY_SFX_CHAIN_END, pSFXToStop, pCreatorGameObject);
                }

                if(mManageableSFXList [i].AudioSourceIsSet()){
                    mAudioSources [mManageableSFXList [i].GetAudioSourceIndex()].Stop ();
                    mAudioSources [mManageableSFXList [i].GetAudioSourceIndex ()].loop = false;
                }
                mManageableSFXList.Remove (mManageableSFXList [i]);
            }
        }
    }

    private void PlayManageableSFXVolume(string pSFXToPlay, GameObject pCreatorGameObject, float pVolume)
    {
        ManageableSFX newManageableSFX = CreateManageableSFX (pSFXToPlay, pCreatorGameObject);
        PlayManageableSFX (newManageableSFX, pVolume);
    }

    private void PlayLoopSFXVolume(string pSFXToPlay, GameObject pCreatorGameObject, float pVolume)
    {
        ManageableSFX newManageableSFX = CreateManageableSFX (pSFXToPlay, pCreatorGameObject);
        newManageableSFX.SetShouldLoop (true);
        PlayManageableSFX (newManageableSFX, pVolume);
    }
}