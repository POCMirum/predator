﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;
using UnityEngine.SceneManagement;

public class RestartButton : RadixMonoBehaviour
{
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
