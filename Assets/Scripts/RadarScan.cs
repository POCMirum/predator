﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

[RequireComponent(typeof(CircleCollider2D))]
public class RadarScan : RadixMonoBehaviour
{
    [SerializeField]
    private float m_MaxSize = 10f;

    [SerializeField]
    private float m_SizeIncrement = 0.035f;

    protected CircleCollider2D mCircleCollider;
    protected Transform mTransform;

    protected float mScale = 0f;

    protected Vector2 mOriginPosition = Vector2.zero;

    private void Start()
    {
        SetDependencies();
    }

    private void SetDependencies()
    {
        mCircleCollider = GetComponent<CircleCollider2D>();
        mTransform = this.transform;
        mOriginPosition = this.transform.position;
        mScale = mTransform.localScale.x;
    }

    private void Update()
    {
        UpdateSize();
    }

    private void UpdateSize()
    {
        if (mScale < m_MaxSize)
        {
            mScale += m_SizeIncrement;
            Vector3 tempScale = new Vector3(mScale, mScale, mTransform.localScale.z);
            mTransform.localScale = tempScale;
            if (mTransform.localScale.x >= m_MaxSize)
            {
                DeleteAtMaxSize();
            }
        }
    }

    private void DeleteAtMaxSize()
    {
        Destroy(this.gameObject);
    }

    public virtual bool CanBeHeard()
    {
        return false;
    }

    public Vector2 GetOrigin()
    {
        return mOriginPosition;
    }
}
