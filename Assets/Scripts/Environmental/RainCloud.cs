﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Radix;

public class RainCloud : RadixMonoBehaviour {

    [SerializeField]
    private float TimeBetweenRainDrops = 0.1f;

    [SerializeField]
    private GameObject m_RadarWavePrefab;

    private Timer mRainDropTimer;

    private EdgeCollider2D mEdgeCollider;

    private float mCloudWidth = 0f;
    private Vector2 mPointA;
    private Vector2 mPointB;

    // Use this for initialization
    void Start () {
        mEdgeCollider = GetComponent<EdgeCollider2D>();
        GetCloudWidth();
        mRainDropTimer = new Timer(TimeBetweenRainDrops, OnRainDrop, true);
        mRainDropTimer.Start();
	}

    protected override void Dispose()
    {
        mRainDropTimer.Dispose();
    }

    private void OnRainDrop()
    {
        Vector2 mRaindropStart = new Vector2(Random.Range(mPointA.x, mPointB.x), mPointA.y);
        DropRain(mRaindropStart);
    }

    private void DropRain(Vector2 pStartPoint)
    {
        Vector2 down = new Vector2(0, -100);
        RaycastHit2D hit;
        RaycastHit2D[] hits = Physics2D.RaycastAll(pStartPoint, down, Mathf.Infinity);
        int hitIndex = -1;
        for(int i = 0; i < hits.Length; i++)
        {
            if(LayerMask.LayerToName(hits[i].collider.gameObject.layer) == "Ghost")
            {
                hitIndex = i;
                break;
            }
        }
        if(hitIndex != -1)
        {
            Instantiate(m_RadarWavePrefab, hits[hitIndex].point, m_RadarWavePrefab.transform.rotation);
        }
    }

    private void GetCloudWidth()
    {
        mPointA = this.transform.TransformPoint(mEdgeCollider.points[0]);
        mPointB = this.transform.TransformPoint(mEdgeCollider.points[mEdgeCollider.points.Length -1]);
        mCloudWidth = Vector2.Distance(mPointA, mPointB);
    }
}
