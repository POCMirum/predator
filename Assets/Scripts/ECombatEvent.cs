﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;

public delegate void PredatorEnemyDelegate(Predator pPredator,Enemy pEnemy);
public delegate void EnemyDelegate(Enemy pEnemy);

[RadixEventEnum]
public enum ECombatEvent
{
    [EventHandlerAttribute(typeof(PredatorEnemyDelegate))]
    SPOTTED_PREDATOR,

    [EventHandlerAttribute()]
    KILL_PREDATOR,

    [EventHandlerAttribute(typeof(EnemyDelegate))]
    KILL_ENEMY,
}
