﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Radix;

public class PhysicsObject : RadixMonoBehaviour {

    [SerializeField]
    private float m_GravityModifier = 1f;
    [SerializeField]
    protected float m_MinGroundNormalY = 0.65f;

    protected const float SHELL_RADIUS = 0.01f;
    protected const float MIN_MOVE_DISTANCE = 0.001f;

    protected Rigidbody2D mRigidBody;

    protected Vector2 velocity;
    
    protected ContactFilter2D mContactFilter;

    protected RaycastHit2D[] mHitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> mHitBufferList = new List<RaycastHit2D>(16);

    protected Vector2 mTargetVelocity;

    protected bool mIsGrounded = false;
    protected Vector2 mGroundNormal = new Vector2();

    private void OnEnable()
    {
        mRigidBody = GetComponent<Rigidbody2D>();
    }

    void Start () {
        mContactFilter.useTriggers = false;
        mContactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
	}
	
	void Update () {
        mTargetVelocity = Vector2.zero;
        ComputeVelocity();
    }

    protected virtual void ComputeVelocity()
    {

    }

    protected virtual void FixedUpdate()
    {
        velocity += m_GravityModifier * Physics2D.gravity * Time.deltaTime;
        velocity.x = mTargetVelocity.x;

        mIsGrounded = false;

        Vector2 moveAlongGround = new Vector2(mGroundNormal.y, -mGroundNormal.x); // on the ground, moveAlongGround is equal to (1,0);

        Vector2 deltaPosition = velocity * Time.deltaTime;

        Vector2 movement = moveAlongGround * deltaPosition.x; //When going right, delaPosition.x = 0.08f (probably 1 times delta time.

        ApplyMovement(movement, false);

        movement = Vector2.up * deltaPosition.y;

        ApplyMovement(movement, true);
    }

    protected virtual void ApplyMovement(Vector2 pMovement, bool pYMovement) //This function should be used twice separately if moving both horizontally and vertically
    {
        float distance = pMovement.magnitude;
        if(distance > MIN_MOVE_DISTANCE)
        {
            int count = mRigidBody.Cast(pMovement, mContactFilter, mHitBuffer, distance + SHELL_RADIUS);
            mHitBufferList.Clear();
            for(int i = 0; i < count; i++)
            {
               mHitBufferList.Add(mHitBuffer[i]);
            }

            for (int i = 0; i < mHitBufferList.Count; i++)
            {
                Vector2 currentNormal = mHitBufferList[i].normal;
                if(currentNormal.y > m_MinGroundNormalY)
                {
                    mIsGrounded = true;
                    if(pYMovement)
                    {
                        mGroundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                float projection = Vector2.Dot(velocity, currentNormal);
                if(projection < 0)
                {
                    velocity = velocity - projection * currentNormal;
                }

                float modifiedDistance = mHitBufferList[i].distance - SHELL_RADIUS;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }
            
        }
        mRigidBody.position = mRigidBody.position + pMovement.normalized * distance;
    }
}
