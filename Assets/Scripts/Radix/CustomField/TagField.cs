﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

public class TagField : RadixCustomField
{
	#if UNITY_EDITOR
    public override void Display(RadixMonoBehaviour pTarget, SerializedObject pSerializedObject, FieldInfo pFieldInfo)
    {
		if(pFieldInfo.GetValue(pTarget) == null || string.IsNullOrEmpty(pFieldInfo.GetValue(pTarget).ToString()))
		{
			pFieldInfo.SetValue (pTarget, "Untagged");
		}

        pFieldInfo.SetValue(pTarget, EditorGUILayout.TagField(GetFormattedFieldName(pFieldInfo), pFieldInfo.GetValue(pTarget).ToString()));
    }
	#endif
}
