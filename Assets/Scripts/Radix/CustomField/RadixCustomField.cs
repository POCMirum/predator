﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class RadixCustomField : System.Attribute
{
    public virtual bool OverrideUnityDefaultInspector {get { return true; }}

	#if UNITY_EDITOR
    public abstract void Display(RadixMonoBehaviour pTarget, SerializedObject pSerializedObject, FieldInfo pFieldInfo);
	#endif

    protected string GetFormattedFieldName(FieldInfo pFieldInfo)
    {
        string fieldName = pFieldInfo.Name;

        if (fieldName.StartsWith("m_"))
        {
            fieldName = fieldName.Remove(0, 2);
        }

        return fieldName;
    }
}
