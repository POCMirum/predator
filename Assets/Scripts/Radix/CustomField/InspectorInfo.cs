﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class InspectorInfo : RadixCustomField
{
    public override bool OverrideUnityDefaultInspector { get { return false; } }

    private string mMessage;

    public InspectorInfo(string pMessage)
    {
        Assert.CheckNullAndEmpty(pMessage);

        mMessage = pMessage;
    }

	#if UNITY_EDITOR
    public override void Display(RadixMonoBehaviour pTarget, SerializedObject pSerializedObject, FieldInfo pFieldInfo)
    {
        EditorGUILayout.HelpBox(mMessage, MessageType.Info);
    }
	#endif
}
