﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System;
using Radix;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

#if UNITY_EDITOR
[CustomEditor(typeof(RadixMonoBehaviour), true)]
public class RadixMonoBehaviousCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        CheckFieldsInfo();
        serializedObject.ApplyModifiedProperties();
    }

    private void CheckFieldsInfo()
    {
        FieldInfo[] fields = GetFieldsInfoFromType(target.GetType());

		HandleFieldInfoList(fields, target.GetType());

        Type baseType = target.GetType().BaseType;
        while (baseType != null)
        {
            FieldInfo[] baseTypefields = GetFieldsInfoFromType(baseType);
            if (baseTypefields.Length > 0 && HaveSerializableField(baseTypefields))
            {
                BeginBaseTypeSection(baseType.Name);
				HandleFieldInfoList(baseTypefields, baseType);
                EndBaseTypeSection();
            }
            baseType = baseType.BaseType;
        }
    }

    private bool HaveSerializableField(FieldInfo[] pFieldInfoList)
    {
        return pFieldInfoList.ToList().Exists((fieldInfo) =>
        {
            return fieldInfo.GetCustomAttributes(typeof(SerializeField), true).Length > 0;
        });
    }

    private FieldInfo[] GetFieldsInfoFromType(Type pType)
    {
		return pType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
    }

	private void HandleFieldInfoList(FieldInfo[] pFieldInfoList, Type pType)
    {
        foreach (FieldInfo info in pFieldInfoList)
		{
			if (info.DeclaringType == pType) 
			{
				HandleFieldInfo (info);
			}
        }
    }

    private void HandleFieldInfo(FieldInfo pInfo)
    {
        object[] attribute = pInfo.GetCustomAttributes(typeof(RadixCustomField), true);

        bool useDefaultUnityInspector = true;

        if (attribute.Length > 0)
        {
            RadixCustomField[] customFields = attribute as RadixCustomField[];

            useDefaultUnityInspector &= !customFields.ToList().Exists((field) =>
            {
                return field.OverrideUnityDefaultInspector;
            });

            TriggerCustomFields(customFields, pInfo);
        }
       

        //IF target handle himself the custom inspector, trigger it (RadixEvent)

        if(useDefaultUnityInspector)
        {
            UseDefaultPropertyField(pInfo);
        }
    }

    private void TriggerCustomFields(RadixCustomField[] pCustomFields, FieldInfo pInfo)
    {
        foreach (RadixCustomField customField in pCustomFields)
        {
            customField.Display(target as RadixMonoBehaviour, serializedObject, pInfo);
        }
    }

    private void UseDefaultPropertyField(FieldInfo pInfo)
    {
        SerializedProperty serializedProperty = serializedObject.FindProperty(pInfo.Name);
        if (serializedProperty != null)
        {
            if (serializedProperty.isArray)
            {
                EditorGUILayout.PropertyField(serializedProperty, true);
            }
            else
            {
                EditorGUILayout.PropertyField(serializedProperty);
            }
        }
    }

    private void BeginBaseTypeSection(string pBaseTypeName)
    {
        var boxStyle = new GUIStyle();
        boxStyle.normal.background = MakeTex(600, 1, new Color(1.0f, 1.0f, 1.0f, 0.1f));
        boxStyle.margin = new RectOffset(0, 0, 10, 10);
        boxStyle.padding = new RectOffset(15, 10, 2, 0);

        var fontStyle = new GUIStyle();
        fontStyle.fontStyle = FontStyle.Bold;
        fontStyle.normal.textColor = Color.white;
        fontStyle.padding = new RectOffset(-10, 0, 0, 0);

        EditorGUILayout.BeginVertical(boxStyle);
        EditorGUILayout.LabelField(pBaseTypeName, fontStyle);
        EditorGUILayout.Space();
    }

    private void EndBaseTypeSection()
    {
        EditorGUILayout.EndVertical();
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }
}
#endif
