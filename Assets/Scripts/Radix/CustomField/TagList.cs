﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class TagList : RadixCustomField
{
	#if UNITY_EDITOR
    public override void Display(RadixMonoBehaviour pTarget, SerializedObject pSerializedObject, FieldInfo pFieldInfo)
    {
        SerializedProperty serializedProperty = pSerializedObject.FindProperty(pFieldInfo.Name);
        serializedProperty.isExpanded = EditorGUILayout.Foldout(serializedProperty.isExpanded, serializedProperty.displayName);

        if (serializedProperty.isExpanded)
        {
            if (ValueIsListOfString(pFieldInfo, pTarget))
            {
                List<string> tagList = pFieldInfo.GetValue(pTarget) as List<string>;

                DisplayListControl(pFieldInfo, pTarget, tagList);
                DisplayButtons(tagList);

                pFieldInfo.SetValue(pTarget, tagList);
            }
            else
            {
                DisplayTypeError();
            }
        }
    }

    private void DisplayTypeError()
    {
        EditorGUILayout.HelpBox("The property is not of type 'List<String>", MessageType.Error);
    }

    private bool ValueIsListOfString(FieldInfo pFieldInfo, RadixMonoBehaviour pTarget)
    {
        return pFieldInfo.GetValue(pTarget) is List<string>;
    }

    private void DisplayListControl(FieldInfo pFieldInfo, RadixMonoBehaviour pTarget, List<string> pTagList)
    {
        if (pTagList.Count > 0)
        {
            EditorGUI.indentLevel++;
            for (int i = 0; i < pTagList.Count; i++)
            {
                pTagList[i] = EditorGUILayout.TagField("Tag #" + (i + 1), pTagList[i]);
            }
            EditorGUI.indentLevel--;
        }
        else
        {
            EditorGUILayout.LabelField("The list is empty.");
        }
    }

    private void DisplayButtons(List<string> pTagList)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button(new GUIContent("+", "Add"), EditorStyles.miniButtonLeft, GUILayout.Width(20f)))
        {
            pTagList.Add(UnityEditorInternal.InternalEditorUtility.tags[0]);
        }

        if (GUILayout.Button(new GUIContent("-", "Remove"), EditorStyles.miniButtonMid, GUILayout.Width(20f))
            && pTagList.Count > 0)
        {
            pTagList.RemoveAt(pTagList.Count - 1);
        }

        if (GUILayout.Button(new GUIContent("Clear", "Clear"), EditorStyles.miniButtonRight)
            && pTagList.Count > 0)
        {
            pTagList.Clear();
        }

        EditorGUILayout.EndHorizontal();
    }

	#endif
}