﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public class RadixEventField : RadixCustomField
{
	#if UNITY_EDITOR
    public override void Display(RadixMonoBehaviour pTarget, SerializedObject pSerializedObject, FieldInfo pFieldInfo)
    {
        List<Type> types = new List<Type>();
        foreach (Assembly ass in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (Type t in ass.GetExportedTypes())
            {
                if (t.IsEnum && t.GetCustomAttributes(typeof(RadixEventEnum), true).Length > 0)
                {
                    types.Add(t);
                }
            }
        }

        RadixEvent TargetEnum = pFieldInfo.GetValue(pTarget) as RadixEvent;

        string[] options = new string[types.Count];

        int index = 0;

        for (int i = 0; i < types.Count; i++)
        {
            options[i] = types[i].Name;

            if (TargetEnum != null && !string.IsNullOrEmpty(TargetEnum.m_EnumName) && TargetEnum.m_EnumName == types[i].ToString())
            {
                index = i;
            }
        }
			
        EditorGUILayout.LabelField(pSerializedObject.FindProperty(pFieldInfo.Name).displayName);

        EditorGUI.indentLevel++;
        EditorGUILayout.BeginHorizontal();

        index = EditorGUILayout.Popup(index, options);

        Enum lol = Activator.CreateInstance(types[index]) as Enum;

        if (TargetEnum == null || TargetEnum.m_EnumName == null || TargetEnum.m_EnumName != types[index].ToString())
        {
            TargetEnum = ScriptableObject.CreateInstance(typeof(RadixEvent)) as RadixEvent;
            TargetEnum.SetEnum(lol);
        }

		if (Application.isEditor && !Application.isPlaying)
        {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }

        //TO DO Need to detech if the enum has really changwe
        TargetEnum.SetEnum(EditorGUILayout.EnumPopup(TargetEnum.m_Enum));

        pFieldInfo.SetValue(pTarget, TargetEnum);

        pSerializedObject.ApplyModifiedProperties();
        EditorGUILayout.EndHorizontal();
        EditorGUI.indentLevel--;
    }
	#endif
}
