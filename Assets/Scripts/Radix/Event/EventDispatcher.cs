﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// Define Complex Type
using EventDictionnary = System.Collections.Generic.Dictionary<System.Enum, System.Collections.Generic.IList<Radix.EventListener>>;
using EventPair = System.Collections.Generic.KeyValuePair<System.Enum, System.Collections.Generic.IList<Radix.EventListener>>;


namespace Radix
{
    internal class EventDispatcher
    {
        private static EventDictionnary mEventDictionnary = new EventDictionnary();

        internal void RegisterEventListener(EventListener pListener)
        {
            Assert.CheckNull(pListener);

            CheckIfEventExist(pListener.Event);
            AddListener(pListener);
        }

        private void CheckIfEventExist(Enum pEvent)
        {
            if (!mEventDictionnary.ContainsKey(pEvent))
            {
                mEventDictionnary[pEvent] = new List<EventListener>();
            }
        }

        private void AddListener(EventListener pListener)
        {
            if (!ContainListener(pListener))
            {
                mEventDictionnary[pListener.Event].Add(pListener);
            }
        }

        private void UnregisterEventListener(EventListener pListener)
        {
            if (pListener != null)
            {
                pListener.Dispose();
                pListener = null;
            }
        }

        internal void UnregisterEventListener(Enum pEvent, System.Object pListenerParent)
        {
            if (mEventDictionnary.ContainsKey(pEvent))
            {
                EventListener eventListener = mEventDictionnary[pEvent].FirstOrDefault((currentEventListener) => 
					{ 
						return currentEventListener.ListenerHashCode == pListenerParent.GetHashCode(); 
					});
                mEventDictionnary[pEvent].Remove(eventListener);
                UnregisterEventListener(eventListener);
            }
        }

		internal void UnregisterEventListener(System.Object pListenerParent)
		{
			foreach (Enum anEvent in mEventDictionnary.Keys) {
				UnregisterEventListener (anEvent, pListenerParent);
			}
		}

        internal void UnregisterAllEventsListeners(Type pEvent)
        {
            List<Enum> eventToRemove = new List<Enum>();
            foreach (EventPair eventPair in mEventDictionnary)
            {
                if(eventPair.Key.GetType() == pEvent)
                {
                    foreach(EventListener listener in eventPair.Value)
                    {
                        UnregisterEventListener(listener);
                    }
                    eventPair.Value.Clear();
                    eventToRemove.Add(eventPair.Key);
                }
            }
        }

		internal void UnregisterAllEventsListeners()
		{
			List<Enum> eventToRemove = new List<Enum>();
			foreach (EventPair eventPair in mEventDictionnary)
			{
				foreach(EventListener listener in eventPair.Value)
				{
					UnregisterEventListener(listener);
				}
				eventPair.Value.Clear();
				eventToRemove.Add(eventPair.Key);
			}
		}

        private void RemoveEvent(List<Enum> pEventToRemove)
        {
            foreach (Enum eventName in pEventToRemove)
            {
                if (mEventDictionnary.ContainsKey(eventName) 
                    && mEventDictionnary[eventName].Count == 0)
                {
                    mEventDictionnary.Remove(eventName);
                }
            }
        }
			
        internal void DispatchEvent(Enum pEvent, params object[] pArgs)
        {
            DispatchEvent(pEvent, null, pArgs);
        }

        internal void DispatchEvent(Enum pEvent, Type _listenerType, params object[] pArgs)
        {
            if (mEventDictionnary.ContainsKey(pEvent))
            {
                foreach (EventListener listener in mEventDictionnary[pEvent])
                {
                    if (ListenerIsValid(listener))
                    {
                        Assert.CheckNull(listener.Callback);
                        InvokeCallback(listener.Callback, pEvent, pArgs);
                    }
                }
            }
        }

        private bool ListenerIsValid(EventListener pListener)
        {
            bool listenerIsValid = true;

            Type listenerType = pListener.Listener.GetType();

            if (listenerType == typeof(MonoBehaviour) || listenerType.IsSubclassOf(typeof(MonoBehaviour)))
            {
                var monoBehaviour = pListener.Listener as MonoBehaviour;

                listenerIsValid = monoBehaviour.isActiveAndEnabled && monoBehaviour.gameObject != null && monoBehaviour.gameObject.activeInHierarchy;
            }

            return listenerIsValid;
        }

		private void InvokeCallback(Delegate pCallback, Enum pEvent, params object[] pArgs )
        {  
			try
            {
				pCallback.DynamicInvoke(pArgs);
            }
            catch(Exception ex)
            {
				
				Error.Create (String.Format ("{0}: Target: {1}, Method: {2}, Event: {3}, Delagate {4}", ex.Message, pCallback.Target, pCallback.Method, pEvent, pCallback), EErrorSeverity.CRITICAL);
            }
        }

        private bool ContainListener(EventListener pListener)
        {
            Assert.CheckNull(pListener);
            return (mEventDictionnary[pListener.Event].FirstOrDefault((eventListener) => { return eventListener.Equals(pListener); }) != null);
        }
    }
}
