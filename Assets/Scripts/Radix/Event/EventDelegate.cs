﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

public delegate void VoidDelegate();
public delegate void IntDelegate(int pArg);
public delegate void StringDelegate(string pArg);
public delegate void BoolDelegate(bool pArg);
public delegate void FloatDelegate(float pArg);
