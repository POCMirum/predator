﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using System;


#if UNITY_WSA || UNITY_WP8 || UNITY_WP8_1
using System.Reflection;
#endif

namespace Radix
{
	[ServiceFlags(ESystemContext.STANDARD)]
	public class EventService : ServiceBase, IEventService
    {
        private EventDispatcher mEventDispatcher;

		public void Init ()
		{
			mEventDispatcher = new EventDispatcher();
        }

		public override void Dispose()
        {
            mEventDispatcher.UnregisterAllEventsListeners();
			mEventDispatcher = null;
        }

		public void Register(Enum pEvent, VoidDelegate pCallback)
		{
			Register<VoidDelegate>(pEvent, pCallback);
		}

        public void Register<T>(Enum pEvent, T pCallback)
		{
			Assert.CheckNull(pCallback);

			if (IsGoodDelegateType(pEvent, typeof(T)))
			{
				RegisterInternal(pEvent, pCallback as Delegate);
			}
			else
			{
				Error.Create("Callback is not a Delegate of the good type", EErrorSeverity.MAJOR);
			}
		}

        private void RegisterInternal(Enum pEvent, Delegate pCallback)
        {
            var listener = new EventListener(pEvent, pCallback);
            mEventDispatcher.RegisterEventListener(listener);

            Log.Info(pCallback.Target.GetType() + " is listening " + pEvent, ELogCategory.RADIX);
        }
			
		public void UnregisterAllEventListener()
		{
			mEventDispatcher.UnregisterAllEventsListeners();
		}

        public void UnregisterAllEventListener(Type pEvent)
        {
            mEventDispatcher.UnregisterAllEventsListeners(pEvent);
        }

        public void UnregisterEventListener(Enum pEvent, System.Object pListenerParent)
        {
            mEventDispatcher.UnregisterEventListener(pEvent, pListenerParent);
        }

		public void UnregisterEventListener(System.Object pListenerParent)
		{
			mEventDispatcher.UnregisterEventListener(pListenerParent);
		}

        public void DispatchEvent(Enum pEvent, params object[] pArgs)
        {
            if (!(pEvent is EInternalEvent))
            {
                Log.Info("Dispatch event : " + pEvent, ELogCategory.RADIX);
            }

			mEventDispatcher.DispatchEvent(pEvent, pArgs);
            
			mEventDispatcher.DispatchEvent(EInternalEvent.EVENT_DISPATCHED, pEvent);
        }

        private static bool IsGoodDelegateType(Enum pEvent, Type pType)
        {
            Type type = null;
            try
            {
                type = pEvent.GetAttribute<EventHandlerAttribute>().Handler;
            }
            catch (Exception ex)
            {
                Error.Create(ex.Message, EErrorSeverity.MAJOR);
            }

            return type!= null && type == pType;
        }
    }
}
