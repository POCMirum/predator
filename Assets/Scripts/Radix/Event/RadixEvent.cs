﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class RadixEvent : ScriptableObject
{
    [SerializeField] public Enum m_Enum;

    [SerializeField] public string m_EnumName = string.Empty;

    [SerializeField] public string m_EnumValue = string.Empty;

    public void SetEnum(Enum pEnum)
    {
        m_Enum = pEnum;
        m_EnumName = pEnum.GetType().ToString();
        m_EnumValue = pEnum.ToString();
    }

    public void OnEnable()
    {
        if (!string.IsNullOrEmpty(m_EnumValue) && !string.IsNullOrEmpty(m_EnumName))
        {
            Type type = Type.GetType(m_EnumName);
            SetEnum(Enum.Parse(type, m_EnumValue) as Enum);
            Debug.Log(m_EnumValue);
        }
    }
}
