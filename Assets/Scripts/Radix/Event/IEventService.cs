﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System;

namespace Radix
{
    public interface IEventService : IService
    {
        void Register(Enum pEvent, VoidDelegate pCallback);
        void Register<T>(Enum pEvent, T pCallback);

        void UnregisterAllEventListener(Type pEvent);
        void UnregisterEventListener(System.Object pListenerParent);
        void UnregisterEventListener(Enum pEvent, System.Object pListenerParent);
        void DispatchEvent(Enum pEvent, params object[] pArgs);
    }
}
