﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */
using Radix;
using System;

namespace Radix
{
    public delegate void EventDispatchedDelegate(Enum pEvent);

    [RadixEventEnum]
    public enum EInternalEvent
    {
        [EventHandlerAttribute()]
        ENGINE_STARTED,

        [EventHandlerAttribute(typeof(FloatDelegate))]
        FRAME_UPDATE,

        [EventHandlerAttribute(typeof(EventDispatchedDelegate))]
        EVENT_DISPATCHED
    }
}