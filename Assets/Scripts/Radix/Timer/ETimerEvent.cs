﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;

namespace Radix
{
    public delegate void TimerDelegate(Timer pTimer);

    public enum ETimerEvent
    {
        [EventHandlerAttribute(typeof(TimerDelegate))]
        TIMER_ENDED,
    }
}
