﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using UnityEngine;
using System.Collections;
using Radix;
using System;

namespace Radix
{
    public class Timer : RadixObject
    {
        private float mCurrentTime = 0f;
        private float mTimeBeforeEnd = 0f;

        private bool mIsRunning = false;

		private bool mAutoRestart = false;

		private bool mIsStopwatch = false;

		private VoidDelegate mCallback;

		public Timer()
			: this(0f, null, false)
		{ }

		public Timer(float pTimeBeforeEnd, VoidDelegate pCallback)
			: this(pTimeBeforeEnd, pCallback, false)
        { }

		public Timer(float pTimeBeforeEnd, VoidDelegate pCallback, bool pAutoRestart) 
			: base()
		{
			if (pTimeBeforeEnd < 0) {
				throw new ArgumentException ("Timer delay must be a positive value");
			} else if (pTimeBeforeEnd == 0) {
				mIsStopwatch = true;
			}
			else {
				mTimeBeforeEnd = pTimeBeforeEnd;
			}
			RegisterEvent<FloatDelegate>(EInternalEvent.FRAME_UPDATE, Update);
			mCallback = pCallback;
			mAutoRestart = pAutoRestart;
		}

		private void Update(float pDeltaTime)
        {
			IncrementTime(pDeltaTime);
        }

		private void IncrementTime(float pDeltaTime)
        {
            if (mIsRunning)
            {
				mCurrentTime += pDeltaTime;
                CheckEnd();
            }
        }

        private void CheckEnd()
        {
			if (mCurrentTime >= mTimeBeforeEnd && !mIsStopwatch)
            {
                InvokeCallback();
				if (!mAutoRestart) {
					Stop ();
				} else {
					Restart ();
				}
            }
        }

        public void Start()
        {
            mIsRunning = true;
        }
			
        public void Pause()
        {
            mIsRunning = false;
        }
			
        public void Stop()
        {
            mIsRunning = false;
            mCurrentTime = 0f;
        }

        private void InvokeCallback()
        {
			if (mCallback != null) {
				mCallback ();
			}
        }

        public bool IsStarted()
        {
            return mIsRunning;
        }

		public void Restart()
		{
			Stop ();
			Start ();
		}

		public float GetCurrentTime()
		{
			return mCurrentTime;
		}
    }
}
