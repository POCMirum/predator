/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using UnityEngine;
using System.Collections;
using Radix;
namespace Radix
{
    public static class Math
    {
        public static bool IsBetweenExclusively(this int value, int min, int max)
        {
            Assert.Check(min < max, "");
            return value > min && value < max;
        }

        public static bool IsBetweenInclusively(this int value, int min, int max)
        {
            return value >= min && value <= max;
        }

        public static bool IsBetweenExclusively(this float value, float min, float max)
        {
            return value > min && value < max;
        }

        public static bool IsBetweenInclusively(this float value, float min, float max)
        {
            return value >= min && value <= max;
        }

        public static float Difference(this float value, float pOtherValue)
        {
            return Mathf.Abs(value - pOtherValue);
        }

        public static bool NearlyEqual(this float value, float pOtherValue, float pEpsilon)
        {
            return pOtherValue - pEpsilon < value && value < pOtherValue + pEpsilon;
        }
    }
}
