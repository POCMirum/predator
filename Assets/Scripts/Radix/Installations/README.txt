        _______  _______  ______   _________         
       (  ____ )(  ___  )(  __  \  \__ __/ |\     /|
       | (    )|| (   ) || (  \  )   ) (   ( \   / )
       | (____)|| (___) || |   ) |   | |    \ (_) / 
       |     __)|  ___  || |   | |   | |     ) _ (  
       | (\ (   | (   ) || |   ) |   | |    / ( ) \ 
       | ) \ \__| )   ( || (__/  )___) (___( /   \ )
       |/   \__/|/     \|(______/ \_______/|/     \|

 ___                      __     _                 _           
| _ ) __ _ ___ ___   ___ / _|   /_\  _ __  __ _ __(_)_ _  __ _ 
| _ \/ _` (_-</ -_) / _ \  _|  / _ \| '  \/ _` |_ / | ' \/ _` |
|___/\__,_/__/\___| \___/_|   /_/ \_\_|_|_\__,_/__|_|_||_\__, |
                                                         |___/ 

Step to install Radix in Unity3D (version 5.4.2p1)
1- Copy the Radix folder somewhere in the Assets folder of your project

2- In Unity3D, go to Edit/Project Settings/Scrip Execution Order.
	Add Radix.RadixSystem before the "Default Time" with the value -100

3- Go to your Unity3D ScripTemplates folder
 (C:\Program Files\Unity\Editor\Data\Resources\ScriptTemplates)
 Copy all file from the Template Folder or Radix into it.

4- Restart Unity
	