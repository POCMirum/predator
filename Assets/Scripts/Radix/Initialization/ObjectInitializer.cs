﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System;
using System.Reflection;

namespace Radix
{
	public class ObjectInitializer {

		public static void Init(Object pObject)
		{
			try
			{
				MethodInfo methodInfo = GetMethodInfo(pObject);

				if (methodInfo != null) 
				{
					IService[] services = GetService (methodInfo.GetParameters ());
					methodInfo.Invoke (pObject, services);
				}
			}
			catch (Exception ex) 
			{
				Log.Error ("ObjectInitializer cannot init an object of type " + pObject.GetType () + ", Message: " + ex.Message);  
			}
		}

		private static MethodInfo GetMethodInfo(Object pObject)
		{
			MethodInfo methodInfo = pObject.GetType ().GetMethod("Init", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);

			if (methodInfo == null) 
			{
				methodInfo = pObject.GetType ().GetMethod ("Init");
			}

			return methodInfo;
		}

		private static IService[] GetService(ParameterInfo[] pParameterInfos)
		{
			IService[] services = new IService[pParameterInfos.Length];

			for(uint i = 0; i < pParameterInfos.Length;i++)
			{
				MethodInfo method = typeof(ServiceManager).GetMethod("GetService");

				method = method.MakeGenericMethod(pParameterInfos[i].ParameterType);

				ServiceManager serviceManager = ServiceManagerSingleton.Instance;
				services[i] = method.Invoke(serviceManager, null) as IService;
			}

			return services;
		}
	}
}
