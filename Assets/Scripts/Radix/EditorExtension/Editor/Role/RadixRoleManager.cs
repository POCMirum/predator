﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2017 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

namespace Radix
{
	public static class RadixRoleManager 
	{
		private const string ROLE_PREFIX = "RadixRole_";

		public static void SetRole(ERadixRole pRole, bool pIsActive)
		{
			EditorPrefs.SetBool (GetRolePrefName(pRole), pIsActive);
		}

		public static bool IsRoleActive(ERadixRole pRole)
		{
			return EditorPrefs.GetBool (GetRolePrefName(pRole), false);
		}

		public static void ActivateAll()
		{
			foreach (ERadixRole role in Enum.GetValues(typeof(ERadixRole)))
			{
				SetRole (role, true);
			}
		}

		public static void DeactivateAll()
		{
			foreach (ERadixRole role in Enum.GetValues(typeof(ERadixRole)))
			{
				SetRole (role, false);
			}
		}

		private static string GetRolePrefName(ERadixRole pRole)
		{
			return ROLE_PREFIX + pRole.ToString();
		}
	}
}
