﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

public class RadixRoleWindow : EditorWindow 
{
    void OnGUI()
    {
        var guiStyle = new GUIStyle();
        guiStyle.fontSize = 14;
        guiStyle.fontStyle = FontStyle.Bold;


        EditorGUILayout.LabelField("Active Role", guiStyle);

		foreach (ERadixRole role in Enum.GetValues(typeof(ERadixRole)))
        {
			bool isActive = RadixRoleManager.IsRoleActive (role);

			isActive = EditorGUILayout.Toggle (role.ToString (), isActive);

			RadixRoleManager.SetRole (role, isActive);
        }

		if (GUILayout.Button("Activate All"))
		{
			RadixRoleManager.ActivateAll ();
		}

		if (GUILayout.Button("Deactivate All"))
		{
			RadixRoleManager.DeactivateAll ();
		}
    }

    [MenuItem("Radix/Role Window")]
    static void Init()
    {
		var window = (RadixRoleWindow)EditorWindow.GetWindow(typeof(RadixRoleWindow));
        window.Show();
    }
}
