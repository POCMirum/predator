﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Radix
{
	public enum ERadixRole {

		PROG,
		DESIGNER,
		ARTIST,
		QA,
		AUDIO
	}
}
