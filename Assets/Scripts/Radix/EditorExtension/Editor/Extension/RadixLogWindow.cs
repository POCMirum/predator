﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

public class RadixLogWindow : EditorWindow 
{

    void OnGUI()
    {
        var guiStyle = new GUIStyle();
        guiStyle.fontSize = 14;
        guiStyle.fontStyle = FontStyle.Bold;
        EditorGUILayout.LabelField("Active Log Categories", guiStyle);

        foreach (ELogCategory val in Enum.GetValues(typeof(ELogCategory)))
        {
            EditorPrefs.SetBool("RadixLogCategory_" + val.ToString(), EditorGUILayout.Toggle("Category_" + val.ToString(), EditorPrefs.GetBool("RadixLogCategory_" + val.ToString())));
        }

        GUILayout.Space(30);
        /*if (GUILayout.Button("Align!"))
            Align();*/

        guiStyle.fontSize = 14;
        guiStyle.fontStyle = FontStyle.Bold;
        EditorGUILayout.LabelField("Active Log Types", guiStyle);

        foreach (ELogType val in Enum.GetValues(typeof(ELogType)))
        {
            EditorPrefs.SetBool(val.ToString(), EditorGUILayout.Toggle(val.ToString(), EditorPrefs.GetBool(val.ToString())));
        }

		if (GUILayout.Button("Activate All"))
		{
			foreach (ELogType val in Enum.GetValues(typeof(ELogType)))
			{
				EditorPrefs.SetBool(val.ToString(), EditorGUILayout.Toggle(val.ToString(), true));
			}
		}

		if (GUILayout.Button("Deactivate All"))
		{
			foreach (ELogType val in Enum.GetValues(typeof(ELogType)))
			{
				EditorPrefs.SetBool(val.ToString(), EditorGUILayout.Toggle(val.ToString(), false));
			}
		}
    }

    [MenuItem("Radix/Log Window")]
    static void Init()
    {
        RadixLogWindow window = (RadixLogWindow)EditorWindow.GetWindow(typeof(RadixLogWindow));
        window.Show();
    }
}
