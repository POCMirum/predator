﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;

public class Trigger : RadixMonoBehaviour, ITrigger 
{
    [SerializeField]
    private TriggerHandler[] m_TriggerHandler;

    [SerializeField]
    private bool m_TriggerOnlyOnce;
    
    protected TriggerLogic mTriggerLogic;

	public virtual void Init()
	{
        mTriggerLogic = new TriggerLogic(this);
	}

    public TriggerHandler[] TriggerHandlerList
    {
        get { return m_TriggerHandler; }
    }

    public bool CanBeTriggeredOnlyOnce
    {
        get { return m_TriggerOnlyOnce; }
    }
}
