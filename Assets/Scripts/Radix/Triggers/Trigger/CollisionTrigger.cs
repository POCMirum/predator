﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Collider2D))]
public class CollisionTrigger : Trigger
{
    [SerializeField, TagList]
    private List<string> m_TagsToHandle = new List<string>();

    protected virtual void OnTriggerEnter2D(Collider2D pCollider2D)
    {
        if (m_TagsToHandle.Contains(pCollider2D.tag))
        {
            mTriggerLogic.Trigger();
        }
    }

    void OnDrawGizmos()
    {
       // Gizmos.color = new Color(1, 0, 0, 0.5F);

        //Gizmos.DrawCube(transform.position, new Vector3(10, 150, 1));
    }
}
