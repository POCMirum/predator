﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;

public class TextWriterTriggerHandler : TriggerHandler 
{
    private TextWriter mTextWriter;

    private void Start()
    {
        mTextWriter = GetComponent<TextWriter>();
    }

    public override void OnTrigger()
    {
        if(mTextWriter.IsDrawing)
        {
            mTextWriter.StopWriting();
        } else
        {
            mTextWriter.Write();
        }
    }
}
