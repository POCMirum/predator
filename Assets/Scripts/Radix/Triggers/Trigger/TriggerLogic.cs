﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;

public class TriggerLogic : RadixObject
{
    private ITrigger mTrigger = null;
    private bool mHasBeenTriggered = false;

    public TriggerLogic(ITrigger pTrigger)
    {
        mTrigger = pTrigger;
    }

    public void Trigger()
    {
        if (CanbeTriggred())
        {
            mHasBeenTriggered = true;
            foreach (var triggerHandler in mTrigger.TriggerHandlerList)
            {
                triggerHandler.OnTrigger();
            }
        }
    }

    private bool CanbeTriggred()
    {
        return !mTrigger.CanBeTriggeredOnlyOnce || !mHasBeenTriggered;
    }
}
