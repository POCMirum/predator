﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using System;

namespace Radix
{
    public class EventTrigger : Trigger
    {
        [SerializeField, RadixEventField]
        private RadixEvent m_EventToListen = null;

        protected virtual void Start()
        {
            RegisterEvent<EventDispatchedDelegate>(EInternalEvent.EVENT_DISPATCHED, OnEventDispatched);
        }

        private void OnEventDispatched(Enum pEvent)
        {
            if ( IsTheListenedEvent(pEvent))
            {
                mTriggerLogic.Trigger();
            }
        }

        private bool IsTheListenedEvent(Enum pEvent)
        {
            return m_EventToListen != null && pEvent.ToString() == m_EventToListen.m_Enum.ToString();
        }
    }
}
