﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2017 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using System;

namespace Radix
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIVisibilityTriggerHandler : TriggerHandler
    {
        [SerializeField]
        private float m_AlphaOnTrigger = 1.0f;

        private CanvasGroup mCanvasGroup;

        protected virtual void Start()
        {
            mCanvasGroup = GetComponent<CanvasGroup>();
        }

        public override void OnTrigger()
        {
            Assert.CheckNull(mCanvasGroup);
            mCanvasGroup.alpha = m_AlphaOnTrigger;
        }
    }
}
