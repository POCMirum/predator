﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;

public class IntegrationTestTriggerHandler : TriggerHandler
{
    [InspectorInfo("'Fail On Trigger' call IntegrationTest.Fail(); instead of Pass();")]
    [SerializeField]
    private bool m_FailOnTrigger = false;

    public override void OnTrigger()
    {
        if (m_FailOnTrigger)
        {
            IntegrationTest.Fail();
        }
        else
        {
            IntegrationTest.Pass();
        }
    }
}
