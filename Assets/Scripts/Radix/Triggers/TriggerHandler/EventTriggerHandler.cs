﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2017 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;
using System.Collections;
using System;

namespace Radix
{
    public class EventTriggerHandler : TriggerHandler
    {
        [SerializeField, RadixEventField]
        private RadixEvent m_EventToThrowWhenTriggered;

        public override void OnTrigger()
        {
            DispatchEvent(m_EventToThrowWhenTriggered.m_Enum);
        }
    }
}
