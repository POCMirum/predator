﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using System;

namespace Radix
{
    public class RadixObject
    {
        private IEventService mEventService;

        public RadixObject()
        {
            RadixSystem.Create();
            ObjectInitializer.Init(this);
            mEventService = ServiceManagerSingleton.Instance.GetService<IEventService>();
        }

        public virtual void Dispose()
        {
            if (mEventService != null)
            {
                mEventService.UnregisterEventListener(this);
            }
        }

        protected void RegisterEvent(Enum pEvent, VoidDelegate pCallback)
        {
            mEventService.Register(pEvent, pCallback);
        }

        protected void RegisterEvent<T>(Enum pEvent, T pCallback)
        {
            mEventService.Register<T>(pEvent, pCallback);
        }

        protected void UnregisterEvent(Enum pEvent)
        {
            mEventService.UnregisterEventListener(pEvent, this);
        }

        protected void DispatchEvent(Enum pEvent, params object[] pArgs)
        {
            mEventService.DispatchEvent(pEvent, pArgs);
        }

        protected void LogDebug(string pMessage)
        {
            Log.Debug(pMessage, ELogCategory.GAMEPLAY);
        }

        protected void LogError(string pMessage)
        {
            Log.Error(pMessage, ELogCategory.GAMEPLAY);
        }

        protected void LogInfo(string pMessage)
        {
            Log.Info(pMessage, ELogCategory.GAMEPLAY);
        }

        protected void LogWarning(string pMessage)
        {
            Log.Warning(pMessage, ELogCategory.GAMEPLAY);
        }
    }
}