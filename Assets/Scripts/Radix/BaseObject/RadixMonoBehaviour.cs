﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using UnityEngine;
using System;

namespace Radix
{
	public class RadixMonoBehaviour : MonoBehaviour {

		private IEventService mEventService;

		protected virtual void Awake()
		{
			RadixSystem.Create ();
			mEventService = ServiceManagerSingleton.Instance.GetService<IEventService> ();
            ObjectInitializer.Init(this);
        }

		protected virtual void OnDestroy()
		{
			if (mEventService != null) {
				mEventService.UnregisterEventListener (this);
			}
            Dispose();
        }

        protected virtual void Dispose()
        {}

        protected bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        protected void RegisterEvent(Enum pEvent, VoidDelegate pCallback)
		{
			mEventService.Register(pEvent, pCallback);
		}

		protected void RegisterEvent<T>(Enum pEvent, T pCallback)
		{
			mEventService.Register<T>(pEvent, pCallback);
		}

		protected void UnregisterEvent(Enum pEvent)
		{
			mEventService.UnregisterEventListener (pEvent, this);
		}

		protected void DispatchEvent(Enum pEvent, params object[] pArgs)
		{
			mEventService.DispatchEvent (pEvent, pArgs);
		}

		protected void LogDebug(string pMessage)
		{
			Log.Debug (pMessage, ELogCategory.GAMEPLAY);
		}

		protected void LogError(string pMessage)
		{
			Log.Error (pMessage, ELogCategory.GAMEPLAY);
		}

		protected void LogInfo (string pMessage)
		{
			Log.Info (pMessage, ELogCategory.GAMEPLAY);
		}

		protected void LogWarning(string pMessage)
		{
			Log.Warning (pMessage, ELogCategory.GAMEPLAY);
		}

	}
}
