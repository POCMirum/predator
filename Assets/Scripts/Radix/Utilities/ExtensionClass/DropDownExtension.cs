﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Radix
{
    public static class DropdownExtension
    {
        public static string SelectedText(this Dropdown pDropdown)
        {
            return pDropdown.options[pDropdown.value].text;
        }
    }
}
