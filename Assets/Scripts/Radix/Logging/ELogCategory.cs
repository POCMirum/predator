﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */
using System;

namespace Radix
{
    public enum ELogCategory
    {
        [LogCategoryAttribute(true)]
        NONE,

		[LogCategoryAttribute(false, "yellow")]
        RADIX,

        [LogCategoryAttribute(true, "orange")]
        GAMEPLAY,

        [LogCategoryAttribute(false)]
        INPUT,

        [LogCategoryAttribute(false, "green")]
        CHARACTER_STATE,

		[LogCategoryAttribute(true, "blue")]
		SIAQO,

		[LogCategoryAttribute(true, "brown")]
		INFO,
    }

    internal class LogCategoryAttribute : Attribute
    {
        internal LogCategoryAttribute(bool pActive)
        {
            Active = pActive;
            Color = "black";
        }
        
        internal LogCategoryAttribute(bool pActive, string pColor)
        {
            Active = pActive;
            Color = pColor;
        }
        internal bool Active { get; private set; }
        internal string Color { get; private set; }
    }
}
