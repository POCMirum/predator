﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

namespace Radix
{
    internal class LogConfig
    {
        internal const int  UNITY_EDITOR_CONSOLE_LOG_LEVEL  = 4;
        internal const int  IN_GAME_CONSOLE_LOG_LEVEL       = 3;
        internal const bool SAVE_LOG_TO_FILE                = false;
    }
}
