﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System;

namespace Radix
{
    public enum ELogType
    {
        ERROR = 0,

        WARNING = 1,

        DEBUG = 2,

        INFO = 3,
    }

}
