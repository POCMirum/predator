﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System.Collections.Generic;

namespace Radix
{
    public static class SystemContext
    {
        private static IList<ESystemContext> mCurrentContexts;

        public static IList<ESystemContext> GetCurrent()
        {
            mCurrentContexts = new List<ESystemContext>();
            mCurrentContexts.Add(ESystemContext.STANDARD);

            //AddDebugReleaseFlags();

            return mCurrentContexts;
        }

        private static void AddDebugReleaseFlags()
        {
#if UNITY_EDITOR
            mCurrentContexts.Add(ESystemContext.DEBUG);
#else
			mCurrentContexts.Add (ESystemContext.RELEASE);
#endif
        }

        private static void AddPlatformSpecificFlags()
        {
#if UNITY_STANDALONE
            mCurrentContexts.Add(ESystemContext.STANDALONE);
#elif UNITY_IOS
            mCurrentContexts.Add(ESystemContext.iOS);
#elif UNITY_ANDROID
            mCurrentContexts.Add(ESystemContext.ANDROID);
#else
			mCurrentContexts.Add (ESystemContext.UNKNOWN_PLATEFORM);
#endif
        }
    }
}
