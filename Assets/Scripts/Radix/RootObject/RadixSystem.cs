﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using UnityEngine;
using System.Collections;
using Radix;

namespace Radix
{
	public class RadixSystem : RadixMonoBehaviour {

		private const string RADIX_SYSTEM_BASE = "RadixSystem";

		private static bool mIsInstantiated = false;
        private static Object mGameObject = null;

		public static void Create()
		{
			if (!mIsInstantiated) {
				mIsInstantiated = true;
                mGameObject = Instantiate(Resources.Load (RADIX_SYSTEM_BASE));
                ServiceManager.Init();
            }
		}

        public static void Destroy()
        {
            mIsInstantiated = false;
            Destroy(mGameObject);
            mGameObject = null;

            ServiceManager.Dispose();
        }

        protected override void Awake()
        {
            ServiceManager.Init();
            base.Awake();
        }

        private void Update() {
			DispatchEvent (EInternalEvent.FRAME_UPDATE, Time.deltaTime);
		}
	}
}
