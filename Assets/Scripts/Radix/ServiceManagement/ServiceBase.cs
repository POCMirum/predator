﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */


namespace Radix 
{
    public abstract class ServiceBase : IService
    {
        public EServiceState State
        {
            get;
            private set;
        }

        internal ServiceBase()
        {
            State = EServiceState.DISPOSED;
        }
        
		public virtual void Dispose() {}

        internal void CallInit()
        {
            if (StateIs(EServiceState.DISPOSED))
            {
                ObjectInitializer.Init(this);
                State = EServiceState.IDLE;
            }
            else
            {
                TrowInvalidStateError();
            }
        }

        internal void CallDispose()
        {
            if (!StateIs(EServiceState.DISPOSED))
            {
                this.Dispose();
                State = EServiceState.DISPOSED;
            }
            else
            {
                TrowInvalidStateError();
            }
        }

        protected bool StateIs(EServiceState pState)
        {
            return State == pState;
        }

        private void TrowInvalidStateError()
        {
            Error.Create("Invalid Service State", EErrorSeverity.CRITICAL);
        }
    }
}
