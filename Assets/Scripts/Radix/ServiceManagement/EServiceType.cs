﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Radix
{
    public enum EServiceType
    {
        STANDARD,
        DEBUG,
        RELEASE,
        PRODUCTION,
        NULL,
        IN_DEVELLOPEMENT
    }

    public class ServiceFlags : Attribute
    {
        private List<ESystemContext> mContextFlags;

		public ServiceFlags(params ESystemContext[] pType)
        {
			mContextFlags = pType.ToList();
        }

		public bool Contains(IList<ESystemContext> pType)
        {
            bool containType = true;

			foreach(ESystemContext type in pType)
            {
				if(!mContextFlags.Contains(type))
                {
                    containType = false;
                }
            }

            return containType;
        }
    }
}
