﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using UnityEngine;
using System.Collections;

namespace Radix
{
	public static class ServiceManagerSingleton {

		private static volatile ServiceManager instance;
		private static object syncRoot = new Object();

		public static ServiceManager Instance
		{
			get
			{
				if (instance == null) //check if singleton exist
				{
					lock (syncRoot) //Double check if the singleton exist (multithreading bug)
					{
						if (instance == null)
							instance = new ServiceManager();
					}
				}

				return instance;
			}
		}
	}
}
