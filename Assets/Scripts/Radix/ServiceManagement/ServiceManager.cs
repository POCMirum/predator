﻿/* -----      MIRUM STUDIO S.E.N.C      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Radix;
using System.Diagnostics;

namespace Radix
{
    public class ServiceManager
    {
        private bool mIsInit = false;
		private List<IService> mServiceList;

		internal ServiceManager() { }

		static public void Init()
		{
            var stackTrace = new StackTrace();
			//TODO Logs shouldn't cause unit tests to fail
            if (IsRadixSystem(stackTrace.GetFrame(1).GetMethod().DeclaringType))
            {
                Log.Info("Init() of the ServiceManager has been called.", ELogCategory.RADIX);
                ServiceManagerSingleton.Instance.InternalInit();
            }
            else
            {
				//TODO this should trigger even if logs are turned off
                Error.Create("Someone other than the Radix System want to Init the ServiceManager", EErrorSeverity.CRITICAL);
            }
        }

        static public void Dispose()
        {
            var stackTrace = new StackTrace();
            if (IsRadixSystem(stackTrace.GetFrame(1).GetMethod().DeclaringType))
            {
                Log.Info("Dispose() of the ServiceManager has been called.", ELogCategory.RADIX);
                ServiceManagerSingleton.Instance.InternalDispose();
            }
            else
            {
                Error.Create("Someone other than the Radix System want to Dispose the ServiceManager", EErrorSeverity.CRITICAL);
            }
        }

        private static bool IsRadixSystem(Type pType)
        {
            return pType == typeof(RadixSystem);
        }

        private void InternalInit()
        {
            if (!mIsInit)
            {
				Log.Info("////////////////////////////////////////////", ELogCategory.RADIX);
				Log.Info("The ServiceManager begin his initialization.", ELogCategory.RADIX);
				mServiceList = new List<IService>();
				ServiceRegistration.RegisterRadixService ();
                ServiceRegistration.RegisterUnityRadixService();
                CreateAllServices();
                InitAllServices();
                mIsInit = true;
                Log.Info("The ServiceManager has been initialized.", ELogCategory.RADIX);
				Log.Info("////////////////////////////////////////////", ELogCategory.RADIX);
            }
        }

		private void CreateAllServices()
		{
			CheckServiceListValidity();

            Dictionary<Type, Type> radixServiceTypes = ServiceRegistration.RadixServicesRegistered;

            foreach (KeyValuePair<Type, Type> type in radixServiceTypes)
            {
                CreateService(type.Key, type.Value);
            }

            Dictionary<Type, Type> serviceTypes = ServiceRegistration.ServicesRegistered;

			foreach (KeyValuePair<Type, Type> type in serviceTypes)
			{
				CreateService(type.Key, type.Value);
			}
		}

		private void CreateService(Type pIServiceType, Type pServiceType)
		{
			if (CanCreateService (pIServiceType, pServiceType))
			{
				IService service = Activator.CreateInstance (pServiceType) as IService;
				mServiceList.Add (service);

				Log.Info(pServiceType.ToString() + " has been created", ELogCategory.RADIX);
			} 
			else
			{
				Log.Info(pServiceType.ToString() + " cannot be create", ELogCategory.RADIX);
			}
		}

		private bool CanCreateService(Type pIServiceType, Type pServiceType)
		{
			IList<ESystemContext> flags = SystemContext.GetCurrent ();

			bool canCreate = true;

			canCreate &= !ServiceTypeAlreadyCreated (pIServiceType);

			object[] customAttributeList = pServiceType.GetCustomAttributes (typeof(ServiceFlags), true);

			canCreate &= (customAttributeList.Length == 1);

			var serviceFlag = customAttributeList.ElementAt (0);

			canCreate &= serviceFlag is ServiceFlags;
			canCreate &= (serviceFlag as ServiceFlags).Contains (flags);

			return canCreate;
		}

		private bool ServiceTypeAlreadyCreated(Type pIServiceType)
		{
			return mServiceList.Exists ((service) => 
				{
					return service.GetType() == pIServiceType;
				});
		}

		private void InitAllServices()
		{
			CheckServiceListValidity();

			foreach (IService service in mServiceList)
			{
				(service as ServiceBase).CallInit();
			}
		}

		public T GetService<T>() where T : IService
		{
			CheckServiceListValidity();

			T serviceBase = (T) mServiceList.FirstOrDefault((service) => service.GetType().GetInterfaces().Contains( typeof(T)));

			Assert.CheckNull(serviceBase, "Cannot find an instance of a " + typeof(T).Name);

			return serviceBase;
		}

        internal void InternalDispose()
        {
            CheckServiceListValidity();

            DisposeAllServices();

            mServiceList.Clear();
            mServiceList = null;

            mIsInit = false;

            Log.Info("ServiceManager disposed", ELogCategory.RADIX);
        }

        internal void DisposeAllServices()
		{
			CheckServiceListValidity();

			foreach (IService service in mServiceList)
			{
				(service as ServiceBase).CallDispose();
			}
		}

        private void CheckServiceListValidity()
        {
            if (mServiceList == null)
            {
                throw new NullReferenceException("The Service Manager contains no service's instance.");
            }
        }
    }
}
