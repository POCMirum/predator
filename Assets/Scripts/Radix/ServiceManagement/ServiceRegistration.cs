﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using Radix;

namespace Radix
{
    public class ServiceRegistration
    {
        static private Dictionary<Type, Type> mRadixServiceTypes = new Dictionary<Type, Type>();

        static internal Dictionary<Type, Type> RadixServicesRegistered
        {
            get { return mRadixServiceTypes; }
        }

        static private Dictionary<Type, Type> mServiceTypes = new Dictionary<Type, Type>();

        static internal Dictionary<Type, Type> ServicesRegistered
        {
            get { return mServiceTypes; }
        }

        public static void RegisterService(Type pServiceInterfaceType, Type pServiceType)
        {
            if (ValidateInterfaceType(pServiceInterfaceType)
               && ValidateServiceType(pServiceType))
            {
                mServiceTypes.Add(pServiceInterfaceType, pServiceType);
            }
        }

        private static bool ValidateInterfaceType(Type pServiceInterfaceType)
        {
            Type[] interfaceList = pServiceInterfaceType.FindInterfaces(new TypeFilter(MyInterfaceFilter), "Radix.IService");

            return interfaceList.Length > 0;
        }

        private static bool ValidateServiceType(Type pServiceType)
        {
            return pServiceType.IsSubclassOf(typeof(ServiceBase));
        }

        public static bool MyInterfaceFilter(Type typeObj, Object criteriaObj)
        {
            return (typeObj.ToString() == criteriaObj.ToString());
        }

        static public void RegisterRadixService()
        {

            mRadixServiceTypes.Add(typeof(IEventService), typeof(EventService));
        }

        static public void RegisterUnityRadixService()
        {
           // RegisterService(typeof(IMouseService), typeof(MouseService));
        }
    }
}
