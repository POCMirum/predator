﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Line
{
    public float DrawSpeed = 3.0f;
    public const int MAX_POINTS_PER_LINE = 30;

    //A list containing all corners of the PolygonCollider2D, to which has been appended the first corner, allowing a linerenderer to close the loop.
    protected List<Vector3> mCornersToDraw = new List<Vector3>();
    //A list of points in space given to the linerenderer to draw the line.
    protected List<Vector3> mPointsToDraw = new List<Vector3>();
    protected float mStartTime = 0f;
    protected float mDistanceToDraw = 0f;

    protected int mStartCornerToDrawIndex = 0;
    protected int mEndCornerToDrawIndex = 1;

    protected LineRenderer mLineRenderer;

    protected bool mShouldErase = false;

    protected bool mShouldLoopForever;

    public Line(LineRenderer pLineRenderer, List<Vector3> pCornersToDraw, bool pShouldLoopForever)
    {
        mLineRenderer = pLineRenderer;
        mCornersToDraw = pCornersToDraw;

        mStartTime = Time.time;
        mDistanceToDraw = Vector3.Distance(mCornersToDraw[mStartCornerToDrawIndex], mCornersToDraw[mEndCornerToDrawIndex]);
        mShouldLoopForever = pShouldLoopForever;
    }

    public void UpdateLine()
    {
        DecideAction();
    }

    private void DecideAction()
    {
        if (mShouldErase)
        {
            EraseOldestPoint();
        }
        else if (GetFractionOfTrajectoryCovered() < 1f)
        {
            DrawNewPoint();
        }
        else
        {
            ChangeTargetCorners();
        }
    }

    private void DrawNewPoint()
    {
        mPointsToDraw.Add(GetNewPointToDraw());
        ResizeLine();
        mLineRenderer.SetPositions(mPointsToDraw.ToArray());
    }

    private void ResizeLine()
    {
        if (mPointsToDraw.Count > MAX_POINTS_PER_LINE)
        {
            mLineRenderer.positionCount = MAX_POINTS_PER_LINE;
            mPointsToDraw.RemoveAt(0);
        }
        else
        {
            mLineRenderer.positionCount = mPointsToDraw.Count;
        }
    }

    private void EraseOldestPoint()
    {
        if (mPointsToDraw.Count > 0)
        {
            mPointsToDraw.RemoveAt(0);
            mLineRenderer.positionCount = mPointsToDraw.Count;
            mLineRenderer.SetPositions(mPointsToDraw.ToArray());
        }
    }

    public float GetFractionOfTrajectoryCovered()
    {
        float distanceCovered = (Time.time - mStartTime) * DrawSpeed;
        float fracDistance = distanceCovered / mDistanceToDraw;
        return fracDistance;
    }

    private Vector3 GetNewPointToDraw()
    {
        Vector3 newPointToDraw = Vector3.Lerp(mCornersToDraw[mStartCornerToDrawIndex], mCornersToDraw[mEndCornerToDrawIndex], GetFractionOfTrajectoryCovered());
        return newPointToDraw;
    }

    private void ChangeTargetCorners()
    {
        if (mEndCornerToDrawIndex < mCornersToDraw.Count - 1)
        {
            mStartCornerToDrawIndex++;
            mEndCornerToDrawIndex++;
            mStartTime = Time.time;
            mDistanceToDraw = Vector3.Distance(mCornersToDraw[mStartCornerToDrawIndex], mCornersToDraw[mEndCornerToDrawIndex]);
        }
        else if (mShouldLoopForever)
        {
            //There is a visual bug here but it's not TOO bad for now
            //mCornersToDraw.Reverse();
            mStartCornerToDrawIndex = 0;
            mEndCornerToDrawIndex = 1;
            mStartTime = Time.time;
            mDistanceToDraw = Vector3.Distance(mCornersToDraw[mStartCornerToDrawIndex], mCornersToDraw[mEndCornerToDrawIndex]);
        }
        else if (mEndCornerToDrawIndex == mCornersToDraw.Count - 1)
        {
            StartErasing();
        }
    }

    public Vector3 GetFarthestPoint()
    {
        return mPointsToDraw[mPointsToDraw.Count - 1];
    }

    public void StartErasing()
    {
        mShouldErase = true;
    }

    public bool IsErased()
    {
        bool isErased = false;
        if (mPointsToDraw.Count == 0)
        {
            isErased = true;
        }
        return isErased;
    }

    public int GetCurrentLineLengthInPointsDrawn()
    {
        return mPointsToDraw.Count;
    }

    public void RestoreCornersToDraw()
    {
        
    }
}
