﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

public class CharacterBorderDrawer : BorderDrawer
{
    protected override void Update()
    {
        base.Update();
        mColliderBounds.Clear();
        SetupColliderBounds();
    }
}