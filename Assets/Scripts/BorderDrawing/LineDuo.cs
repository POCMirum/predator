﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

public class LineDuo : RadixMonoBehaviour
{
    [SerializeField]
    private LineRenderer m_FirstLineRenderer;
    [SerializeField]
    private LineRenderer m_SecondLineRenderer;

    [SerializeField]
    private float m_LineSpeed = 3.0f;

    private Line mFirstLine;
    private Line mSecondLine;

    private bool mShouldDraw = true;

    private bool mShouldLoopForever = false;

    private void Update()
    {
        CheckDeleteSelf();
    }

    private void CheckDeleteSelf()
    {
        if (mShouldDraw == false && mFirstLine.GetCurrentLineLengthInPointsDrawn() == 0 && mSecondLine.GetCurrentLineLengthInPointsDrawn() == 0)
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    public void CreateNewFirstLine(List<Vector3> pCornersToDraw)
    {
        mFirstLine = new Line(m_FirstLineRenderer, pCornersToDraw, mShouldLoopForever);
        mFirstLine.DrawSpeed = m_LineSpeed;
    }

    public void CreateNewSecondLine(List<Vector3> pCornersToDraw)
    {
        mSecondLine = new Line(m_SecondLineRenderer, pCornersToDraw, mShouldLoopForever);
        mSecondLine.DrawSpeed = m_LineSpeed;
    }

    private LineRenderer GetFirstLineRenderer()
    {
        return m_FirstLineRenderer;
    }

    private LineRenderer GetSecondLineRenderer()
    {
        return m_SecondLineRenderer;
    }

    public Line GetFirstLine()
    {
        return mFirstLine;
    }

    public Line GetSecondLine()
    {
        return mSecondLine;
    }

    public void SetShouldDraw(bool pShouldDraw)
    {
        mShouldDraw = pShouldDraw;
    }

    public bool ShouldDraw()
    {
        return mShouldDraw;
    }

    public bool AreLinesFullyDrawn()
    {
        bool areLinesFullyDrawn = false;
        if (mFirstLine.GetCurrentLineLengthInPointsDrawn() == Line.MAX_POINTS_PER_LINE && mSecondLine.GetCurrentLineLengthInPointsDrawn() == Line.MAX_POINTS_PER_LINE)
        {
            areLinesFullyDrawn = true;
        }

        return areLinesFullyDrawn;
    }

    public void StartErasingLines()
    {
        mFirstLine.StartErasing();
        mSecondLine.StartErasing();
    }

    public void RestoreCornersToDraw()
    {
        mFirstLine.RestoreCornersToDraw();
        mSecondLine.RestoreCornersToDraw();
    }

    public void SetShouldLoopForever(bool pShouldLoopForever)
    {
        mShouldLoopForever = pShouldLoopForever;
    }
}
