﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

[RequireComponent(typeof(PolygonCollider2D))]
public class BorderDrawer : RadixMonoBehaviour
{

    public const string RADAR_TAG_NAME = "Radar";
    //This value is used to account for unity's inaccuracy. If the radar collider's collision detection was set to discrete, this value would need to be much higher.
    private const float UNITY_IMPRECISION_OFFSET = 0.05f;

    //This is an arbitrary value to see if two lines are on top of each other.
    private const float OVERLAP_DISTANCE = 0.05f;
    
    [SerializeField]
    private GameObject m_LineDuoPrefab;

    [SerializeField]
    private bool m_ShouldLinesLoopForever = false;
    
    [SerializeField]
    private bool m_StartOnAwake = false;

    protected const float DRAW_SPEED = 3.0f;
    protected const int MAX_POINTS_PER_LINE = 30;

    protected PolygonCollider2D mPolygonCollider;

    //A list containing all corners of the PolygonCollider2D.
    protected List<Vector3> mColliderBounds = new List<Vector3>();

    protected bool mShouldDraw = false;

    //A list containing all corners of the PolygonCollider2D, to which has been appended the first corner, allowing a linerenderer to close the loop.
    protected List<Vector3> mCornersToDraw = new List<Vector3>();
    protected List<Vector3> mReversedCornersToDraw = new List<Vector3>();

    //protected LineDuo mLineDuo;
    protected List<LineDuo> mLineDuos = new List<LineDuo>();

    protected Vector3 mStartingPoint = Vector3.zero;

    private bool mPausedDrawing = false;

    public void Init()
    {
		
    }

    protected override void Awake()
    {
        SetDependencies();
        if(m_StartOnAwake)
        {
            StartDrawingAtRandomPoint();
        }
        base.Awake();
    }

    public void StartDrawingAtRandomPoint()
    {
        mStartingPoint = mColliderBounds[0];
        StartDrawing();
    }

    public void GhostDetectedCollision(Collision2D pCollision)
    {
        if (pCollision.gameObject.CompareTag(RADAR_TAG_NAME) && pCollision.contacts.Length > 0) //Maybe I don't need this if nor this else if
        {
            mStartingPoint = pCollision.contacts[0].point;
            StartDrawing(pCollision.contacts[0].normal);
        } else if (pCollision.gameObject.GetComponent<Predator>() != null)
        {
            mStartingPoint = pCollision.contacts[0].point;
            StartDrawing(pCollision.contacts[0].normal);
        }
    }

    private void SetDependencies()
    {
        mPolygonCollider = GetComponent<PolygonCollider2D>();
        SetupColliderBounds();
    }

    protected void SetupColliderBounds()
    {
        Vector2[] tempColliderBounds = mPolygonCollider.points;
        for (int i = 0; i < tempColliderBounds.Length; i++)
        {
            Vector3 localPosition = this.transform.TransformPoint(tempColliderBounds[i]);
            localPosition.x += mPolygonCollider.offset.x * this.transform.lossyScale.x;
            localPosition.y += mPolygonCollider.offset.y * this.transform.lossyScale.y;
            mColliderBounds.Add(localPosition);
        }
    }
    
    private void StartDrawing()
    {
        StartDrawing(new Vector2(0, -1));
    }

    private void StartDrawing(Vector2 pCollisionNormal)
    {
        mShouldDraw = true;
            DetermineOrderOfCorners(pCollisionNormal);
            CreateLines();
        mPausedDrawing = false;
    }

    private void DetermineOrderOfCorners(Vector2 pCollisionNormal)
    {
        int indexOfFirstCorner = GetIndexOfFirstCorner(pCollisionNormal);

        SetupCornersToDraw(indexOfFirstCorner);

        SetupReversedCornersToDraw(indexOfFirstCorner);
    }

    private void CreateLines()
    {
        LineDuo newLineDuo = ((GameObject)Instantiate(m_LineDuoPrefab, this.transform)).GetComponent<LineDuo>();
        newLineDuo.SetShouldLoopForever(m_ShouldLinesLoopForever);
        newLineDuo.CreateNewFirstLine(mCornersToDraw);
        newLineDuo.CreateNewSecondLine(mReversedCornersToDraw);
        mLineDuos.Add(newLineDuo);
        ClearCornersToDraw();


        //Testing area
    }

    protected virtual void Update()
    {
        CheckDraw();
    }

    private void CheckDraw()
    {
        if(mPausedDrawing)
        {
            EraseLines();
        }
        if (mShouldDraw)
        {
            Draw();
        }
    }

    public void PauseDrawing()
    {
        mPausedDrawing = true;
    }

    private void Draw()
    {
        for (int i = 0; i < mLineDuos.Count; i++)
        {
            if (mLineDuos[i].ShouldDraw())
            {
                if (mLineDuos[i].GetFirstLine() != null)
                {
                    mLineDuos[i].GetFirstLine().UpdateLine();
                }

                if (mLineDuos[i].GetSecondLine() != null)
                {
                    mLineDuos[i].GetSecondLine().UpdateLine();
                }
            }
        }
        if (!m_ShouldLinesLoopForever)
        {
            EraseLinesIfCompleted();
        }
    }

    protected int GetIndexOfFirstCorner(Vector2 pCollisionNormal)
    {
        int indexOfFirstCorner = -1;
        for (int i = 0; i < mColliderBounds.Count - 1; i++)
        {
            if (mStartingPoint == mColliderBounds[i])
            {
                indexOfFirstCorner = i;
            } else if (mStartingPoint == mColliderBounds[i+1])
            {
                indexOfFirstCorner = i + 1;
            } else if (IsPointBetweenPoints(mColliderBounds[i], mColliderBounds[i + 1], mStartingPoint))
            {
                indexOfFirstCorner = i + 1;
            }
        }
        if (IsPointBetweenPoints(mColliderBounds[mColliderBounds.Count - 1], mColliderBounds[0], mStartingPoint))
        {
            indexOfFirstCorner = 0;
        }

        if (indexOfFirstCorner == -1)
        {
            indexOfFirstCorner = GetDefaultIndexOfFirstCorner(pCollisionNormal);
        }

        return indexOfFirstCorner;
    }

    protected void SetupCornersToDraw(int pIndexOfFirstCorner)
    {
        mCornersToDraw.Add(mStartingPoint);
        //Add corners starting from pIndexOfFirstCorner
        for (int i = pIndexOfFirstCorner; i < mColliderBounds.Count; i++)
        {
            mCornersToDraw.Add(mColliderBounds[i]);
        }
        //Add corners previously skipped
        for (int i = 0; i < pIndexOfFirstCorner; i++)
        {
            mCornersToDraw.Add(mColliderBounds[i]);
        }
        mCornersToDraw.Add(mStartingPoint);
    }

    private void SetupReversedCornersToDraw(int pIndexOfFirstCorner)
    {
        mReversedCornersToDraw.Add(mStartingPoint);
        pIndexOfFirstCorner--;
        if (pIndexOfFirstCorner < 0)
        {
            pIndexOfFirstCorner = mColliderBounds.Count - 1;
        }
        //Add corners starting from indexOfFirstCorner
        for (int i = pIndexOfFirstCorner; i >= 0; i--)
        {
            mReversedCornersToDraw.Add(mColliderBounds[i]);
        }
        //Add corners previously skipped
        for (int i = mColliderBounds.Count - 1; i > pIndexOfFirstCorner; i--)
        {
            mReversedCornersToDraw.Add(mColliderBounds[i]);
        }
        mReversedCornersToDraw.Add(mStartingPoint);
    }

    //This is essential since sometimes Unity predicts a collision point, resulting in a point that is outside the actual polygon borders
    private int GetDefaultIndexOfFirstCorner(Vector2 pCollisionNormal)
    {
        int indexOfFirstCorner = -1;
        if(pCollisionNormal.x != 0)
        {
            //Collision happened on the x axis, so I need to find which two corners have the closest x value to the collision contact point.
            SetAdjustedXPositionToStartingPoint();
            indexOfFirstCorner = GetIndexOfFirstCorner(pCollisionNormal); //We recall it here since we just adjusted the "mStartingPoint" so that we should no longer need a "default index of first corner"

        } else if (pCollisionNormal.y != 0)
        {
            //Collision happened on the y axis, so I need to find which two corners have the closest y value to the collision contact point.
            Vector3 tempStartingPoint = mStartingPoint;
            SetAdjustedYPositionToStartingPoint();
            indexOfFirstCorner = GetIndexOfFirstCorner(pCollisionNormal); //We recall it here since we just adjusted the "mStartingPoint" so that we should no longer need a "default index of first corner"
        }
        
        return indexOfFirstCorner;
    }

    private bool IsPointBetweenPoints(Vector3 pFirstExtreme, Vector3 pOtherExtreme, Vector3 pPointToCompare)
    {
        bool isPointBetweenPoints = true;
        
        //Verifies if pPointToCompare is aligned with the two other points. Unfortunately, Unity's collision isn't accurate enough for this check.
        float crossProduct = ((pPointToCompare.y - pFirstExtreme.y) * (pOtherExtreme.x - pFirstExtreme.x) - (pPointToCompare.x - pFirstExtreme.x) * (pOtherExtreme.y - pFirstExtreme.y));
        if (Mathf.Abs(crossProduct) > UNITY_IMPRECISION_OFFSET)
        {
            isPointBetweenPoints = false;
        }

        float dotProduct = (pPointToCompare.x - pFirstExtreme.x) * (pOtherExtreme.x - pFirstExtreme.x) + (pPointToCompare.y - pFirstExtreme.y) * (pOtherExtreme.y - pFirstExtreme.y);
        if (dotProduct < 0)
        {
            isPointBetweenPoints = false;
        }

        float squareLengthTwoExtremes = (pOtherExtreme.x - pFirstExtreme.x) * (pOtherExtreme.x - pFirstExtreme.x) + (pOtherExtreme.y - pFirstExtreme.y) * (pOtherExtreme.y - pFirstExtreme.y);
        if (dotProduct > squareLengthTwoExtremes)
        {
            isPointBetweenPoints = false;
        }

        return isPointBetweenPoints;
    }

    private void EraseLinesIfCompleted()
    {
        for (int i = 0; i < mLineDuos.Count; i++)
        {
            if (!mLineDuos[i].GetFirstLine().IsErased() && !mLineDuos[i].GetSecondLine().IsErased() && AreLinesOverlapping(mLineDuos[i]))
            {
                mLineDuos[i].StartErasingLines();
            }
            else if (mLineDuos[i].GetFirstLine().IsErased() && mLineDuos[i].GetSecondLine().IsErased())
            {
                mLineDuos[i].SetShouldDraw(false);
            }
        }
    }

    private void EraseLines()
    {
        for (int i = 0; i < mLineDuos.Count; i++)
        {
            if (!mLineDuos[i].GetFirstLine().IsErased() && !mLineDuos[i].GetSecondLine().IsErased())
            {
                mLineDuos[i].StartErasingLines();
            }
            else if (mLineDuos[i].GetFirstLine().IsErased() && mLineDuos[i].GetSecondLine().IsErased())
            {
                mLineDuos[i].SetShouldDraw(false);
            }
        }
    }

    private bool AreLinesOverlapping(LineDuo pLineDuo)
    {
        bool areLinesOverlapping = false;
        if (pLineDuo.AreLinesFullyDrawn())
        {
            if (Vector3.Distance(pLineDuo.GetFirstLine().GetFarthestPoint(), pLineDuo.GetSecondLine().GetFarthestPoint()) < OVERLAP_DISTANCE)
            {
                areLinesOverlapping = true;
            }
        }
        return areLinesOverlapping;
    }

    private void ClearCornersToDraw()
    {
        mCornersToDraw = new List<Vector3>();
        mReversedCornersToDraw = new List<Vector3>();
    }


    private void SetAdjustedXPositionToStartingPoint()
    {
        //We take the x value of mStarting point which is NOT on the volume's surface, and change it for the closest X that IS on the volume's surface.
        float closestXValue = mStartingPoint.x;
        float shortestDistance = Mathf.Infinity;
        for(int i = 0; i < mColliderBounds.Count; i++)
        {
            float distance = Mathf.Abs(mColliderBounds[i].x - mStartingPoint.x);
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestXValue = mColliderBounds[i].x;
            }
        }
        mStartingPoint.x = closestXValue;
    }

    private void SetAdjustedYPositionToStartingPoint()
    {
        //We take the y value of mStarting point which is NOT on the volume's surface, and change it for the closest y that IS on the volume's surface.
        float closestYValue = mStartingPoint.y;
        float shortestDistance = Mathf.Infinity;
        for (int i = 0; i < mColliderBounds.Count; i++)
        {
            float difference = mColliderBounds[i].y - mStartingPoint.y;
            float distance = Mathf.Abs(difference);
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestYValue = mColliderBounds[i].y;
            }
        }
        mStartingPoint.y = closestYValue;
    }

    public void SetLineDuoPrefab(LineDuo pLineDuo)
    {
        m_LineDuoPrefab = pLineDuo.gameObject;
    }
}
