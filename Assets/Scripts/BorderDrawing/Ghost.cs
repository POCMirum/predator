﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Radix;
using System.Reflection;
using System;

public class Ghost : RadixMonoBehaviour {

    public const string RADAR_TAG_NAME = "Radar";
    
    private BorderDrawer mAvatarBorderDrawer;

    protected Collider2D mCollider;

    // Use this for initialization
    void Start () {
        
        mCollider = GetComponent<PolygonCollider2D>();
        mAvatarBorderDrawer = GetComponentInParent<BorderDrawer>();
        SetLayerToGhost();
        CopyParentCollider();
    }

    private void SetLayerToGhost()
    {
        this.gameObject.layer = LayerMask.NameToLayer("Ghost");
    }

    private void CopyParentCollider()
    {
        Collider2D parentCollider = this.transform.parent.GetComponent<Collider2D>();
        mCollider = GetCopyOf(parentCollider);
        mCollider.enabled = true;
    }

    private void OnCollisionEnter2D(Collision2D pCollision)
    {
        if (pCollision.gameObject.CompareTag(RADAR_TAG_NAME) || pCollision.gameObject.GetComponent<Predator>() != null)
        {
            if (pCollision.gameObject.CompareTag(RADAR_TAG_NAME))
            {
                Physics2D.IgnoreCollision(pCollision.collider, mCollider);
            }
            ForceAvatarDraw(pCollision);
        }
    }
    
    private void OnCollisionStay2D(Collision2D pCollision)
    {
        Predator predator = pCollision.gameObject.GetComponent<Predator>();
        if (predator != null && predator.CanTouchSense)
        {
            ForceAvatarDraw(pCollision);
            predator.CanTouchSense = false;
            predator.TouchSenseDelayTimer.Restart();
        }
    }

    public T GetCopyOf<T>(T other) where T : Component
    {
        Type type = mCollider.GetType();
        if (type != other.GetType()) return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(mCollider, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(mCollider, finfo.GetValue(other));
        }
        return mCollider as T;
    }

    public void ForceAvatarDraw(Collision2D pCollision)
    {
        mAvatarBorderDrawer.GhostDetectedCollision(pCollision);
    }
}
