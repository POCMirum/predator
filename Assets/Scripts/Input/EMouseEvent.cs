﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;


public delegate void MouseClickDelegate(Vector3 pClickPosition, EMouseButton pClickedButton);

public enum EMouseEvent
{
	[EventHandlerAttribute(typeof(MouseClickDelegate))]
	CLICK,

    [EventHandlerAttribute(typeof(MouseClickDelegate))]
    UP,
}
