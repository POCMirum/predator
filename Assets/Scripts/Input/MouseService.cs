﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2016 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */
 
using System;
using System.Collections.Generic;
using UnityEngine;
using Radix;

[ServiceFlags(ESystemContext.STANDARD)]
public class MouseService : ServiceBase, IMouseService
{
    private IEventService mEventService;

    private Vector2 mLeftClickStartPosition;
    private DateTime mLeftClickStartTime;

	public void Init(IEventService pEventService)
    {
        mEventService = pEventService;
        mEventService.Register (EInternalEvent.FRAME_UPDATE, Update);
	}

	protected virtual void Update()
	{
		try
		{
			HandleClicks();
            HandleUp();
		}
		catch(Exception ex)
		{
			Error.Create(ex.Message, EErrorSeverity.CRITICAL);
		}
	}

	private void HandleClicks()
	{
		if (Input.GetMouseButtonDown (0))
        {
            mLeftClickStartPosition = GetMousePosition();
            mLeftClickStartTime = DateTime.Now;
            DisptachMouseEventClick(EMouseButton.LEFT);
		}
        else if (Input.GetMouseButtonDown (1))
        {
            DisptachMouseEventClick(EMouseButton.RIGHT);
		}
        else if (Input.GetMouseButtonDown (2))
        {
            DisptachMouseEventClick(EMouseButton.MIDDLE);
		}
	}

    private void HandleUp()
    {
        if (Input.GetMouseButtonUp(0))
        {
            SimulateSwipe();
            DisptachMouseEventUp(EMouseButton.LEFT);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            DisptachMouseEventUp(EMouseButton.RIGHT);
        }
        else if (Input.GetMouseButtonUp(2))
        {
            DisptachMouseEventUp(EMouseButton.MIDDLE);
        }
    }

    private void DisptachMouseEventClick(EMouseButton pMouseButton)
    {
        mEventService.DispatchEvent(EMouseEvent.CLICK, Input.mousePosition, pMouseButton);
    }

    private void DisptachMouseEventUp(EMouseButton pMouseButton)
    {
        mEventService.DispatchEvent(EMouseEvent.UP, Input.mousePosition, pMouseButton);
    }

    public Vector2 GetMousePosition()
	{
        Vector3 mousePosition = Input.mousePosition;

        return new Vector2(mousePosition.x, mousePosition.y);
	}

    private void SimulateSwipe()
    {
#if UNITY_EDITOR
        Vector2 currentMousePosition = GetMousePosition();

        float biggestDistance = 0;
        var direction = ESwipeDirection.NONE;

        if(currentMousePosition.x - mLeftClickStartPosition.x > biggestDistance)
        {
            biggestDistance = currentMousePosition.x - mLeftClickStartPosition.x;
            direction = ESwipeDirection.RIGHT;
        }

        if (mLeftClickStartPosition.x - currentMousePosition.x  > biggestDistance)
        {
            biggestDistance = mLeftClickStartPosition.x - currentMousePosition.x;
            direction = ESwipeDirection.LEFT;
        }

        if (currentMousePosition.y - mLeftClickStartPosition.y > biggestDistance)
        {
            biggestDistance = currentMousePosition.y - mLeftClickStartPosition.y;
            direction = ESwipeDirection.UP;
        }

        if (mLeftClickStartPosition.y - currentMousePosition.y > biggestDistance)
        {
            biggestDistance = mLeftClickStartPosition.y - currentMousePosition.y;
            direction = ESwipeDirection.DOWN;
        }

        if(direction != ESwipeDirection.NONE && biggestDistance > Screen.height / 20)
        {
            mEventService.DispatchEvent(ETouchEvent.SWIPE_END, direction, biggestDistance);
        }
#endif
    }
}

