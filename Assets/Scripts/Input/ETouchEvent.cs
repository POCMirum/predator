﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;

public delegate void SwipeBeginHandler(ESwipeDirection pDirection, Vector2 pPosition);
public delegate void SwipeEndHandler(ESwipeDirection pDirection, float pDistance);

public enum ETouchEvent
{
    [EventHandlerAttribute(typeof(SwipeBeginHandler))]
    SWIPE_BEGIN,

    [EventHandlerAttribute(typeof(SwipeEndHandler))]
    SWIPE_END,

    [EventHandlerAttribute(typeof(Vector2Delegate))]
    TAP,

    [EventHandlerAttribute(typeof(Vector2Delegate))]
    DOUBLE_TAP,

    [EventHandlerAttribute()]
    END,

    [EventHandlerAttribute()]
    MOVE
}
