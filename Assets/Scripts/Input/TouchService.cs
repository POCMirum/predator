﻿/* -----      MIRUM STUDIO      -----
 * Copyright (c) 2015 All Rights Reserved.
 * 
 * This source is subject to a copyright license.
 * For more information, please see the 'LICENSE.txt', which is part of this source code package.
 */

using Radix;
using UnityEngine;

#if UNITY_IOS
public class TouchService : iOSTouchService
#else
public class TouchService : DefaultTouchService
#endif
{
    public Vector2 CurrentTouchPosition
    {
        get
        {
			if (mTouchControl != null) {
				return mTouchControl.Position;
			} else {
				return Vector2.zero;
			}
        }
    }

    #region TouchBegan
    protected override void OnTouchBegan(Touch pTouch)
    {
        if(mTouchControl == null)
        {
            mTouchControl = new TouchControl(pTouch);

            HandlerNewTouch();
        }
    }

    private void HandlerNewTouch()
    {
        if (mTouchControl.IsDoubleTap)
        {
            DispatchDoubleTapEvent();
        }
        else
        {
            DispatchTapEvent();
        }
    }

    private void DispatchTapEvent()
    {
        mEventService.DispatchEvent(ETouchEvent.TAP, mTouchControl.Position);
        Log.Debug("Touch Begin: TAP", ELogCategory.INPUT);
    }

    private void DispatchDoubleTapEvent()
    {
        mEventService.DispatchEvent(ETouchEvent.DOUBLE_TAP, mTouchControl.Position);
        Log.Debug("Touch begin: DOUBLE TAP", ELogCategory.INPUT);
    }

    #endregion

    #region TouchMoved

    protected override void OnTouchMoved(Touch pTouch)
    {
        if(IsCurrentTouch(pTouch))
        {
            mTouchControl.UpdateTouch(pTouch);

			bool oldSwipe = mTouchControl.IsSwiping;

            mTouchControl.Moved();
            if(!oldSwipe && mTouchControl.IsSwiping)
            {
                DispatchSwipeBeginEvent(pTouch);
            }
        }
    }

	private void DispatchSwipeBeginEvent(Touch pTouch)
    {
        mEventService.DispatchEvent(ETouchEvent.SWIPE_BEGIN, mTouchControl.SwipeDirection, pTouch.position);
        Log.Debug("SWIPE_BEGIN, " + mTouchControl.SwipeDirection.ToString(), ELogCategory.INPUT);
    }

    #endregion

    #region TouchStationary

    protected override void OnTouchStationary(Touch pTouch)
    {
        if (IsCurrentTouch(pTouch))
        {
            mTouchControl.UpdateTouch(pTouch);
            mTouchControl.OnStationary();
        }
    }

    #endregion

    #region TouchEnded

    protected override void OnTouchEnded(Touch pTouch)
    {
        //check touch null.. Dispose ???
        if (IsCurrentTouch(pTouch))
        {
            EndSwipe();
            mTouchControl = null;
            DispatchTouchEnded();
        }
    }

    private void DispatchTouchEnded()
    {
        mEventService.DispatchEvent(ETouchEvent.END);
        Log.Debug("TOUCH END", ELogCategory.INPUT);
    }

    #endregion

    #region TouchCanceled

    protected override void OnTouchCanceled(Touch pTouch)
    {
        //Touch cancelled
        OnTouchEnded(pTouch);
    }

    #endregion

    #region Private Utilities Method

    private bool IsCurrentTouch(Touch pTouch)
    {
		return mTouchControl != null && mTouchControl.ID == pTouch.fingerId;
    }

    private void UpdateCurrentTouch(Touch pTouch)
    {
        mTouchControl.UpdateTouch(pTouch);
    }

    private void EndSwipe()
    {
        if(mTouchControl.IsSwiping)
        {
            DispatchSwipeEndEvent();
            mTouchControl.EndSwipe();
        }
    }

    private void DispatchSwipeEndEvent()
    {
        mEventService.DispatchEvent(ETouchEvent.SWIPE_END, mTouchControl.SwipeDirection, mTouchControl.SwipeDistance);
        Log.Debug("SWIPE_END, " + mTouchControl.SwipeDistance.ToString(), ELogCategory.INPUT);
    }
    #endregion
}
