﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineMover : SplinePath
{
    [Header("Spline Mover")]
    public int amount = 1;
    public GameObject objectToPlace;

    [Header("Movement")]
    public float Speed;
    public MovementType movementType;

    public List<SplineAgent> passengers;

    public void PlaceObjects()
    {
        for (int i = 0; i < passengers.Count; ++i)
        {
            DestroyImmediate(passengers[i].transform.gameObject);
        }
        passengers.Clear();

        float totalDistance = GetTotalDistance();
        float distanceBetween = totalDistance / Mathf.Max(amount, 1);
        float distanceFromStart = 0f;

        for (int i = 0; i < amount; ++i)
        {
            GameObject instance = GameObject.Instantiate(objectToPlace, GetPosition(distanceFromStart), Quaternion.identity, this.transform);
            SplineAgent agent = new SplineAgent
            {
                transform = instance.transform,
                distanceFromStart = distanceFromStart
            };
            passengers.Add(agent);

            distanceFromStart += distanceBetween;
        }
    }

    private void Update()
    {
        float step = Speed * Time.deltaTime;
        float totalDistance = GetTotalDistance();

        for (int i = passengers.Count - 1; i >= 0; --i)
        {
            if (passengers[i].transform == null)
            {
                passengers.RemoveAt(i);
            }
        }

        for (int i = 0; i < passengers.Count; ++i)
        {
            float direction = passengers[i].forward ? 1f : -1f;

            if (movementType == MovementType.Loop)
            {
                passengers[i].distanceFromStart += step * direction;
            }
            else if (movementType == MovementType.Clamp)
            {
                passengers[i].distanceFromStart = Mathf.Clamp(passengers[i].distanceFromStart + step * direction, 0f, totalDistance);
            }
            else if (movementType == MovementType.PingPong)
            {
                if (passengers[i].forward && passengers[i].distanceFromStart + step > totalDistance)
                {
                    float stepback = passengers[i].distanceFromStart + step - totalDistance;
                    passengers[i].distanceFromStart = totalDistance - stepback;
                    passengers[i].forward = false;
                    passengers[i].ReverseOrientation();
                }
                else if (!passengers[i].forward && (passengers[i].distanceFromStart - step) < 0f)
                {
                    float stepforward = Mathf.Abs(passengers[i].distanceFromStart - step);
                    passengers[i].distanceFromStart = stepforward;
                    passengers[i].forward = true;
                    passengers[i].ReverseOrientation();
                }
                else
                {
                    passengers[i].distanceFromStart = Mathf.Clamp(passengers[i].distanceFromStart + step * direction, 0f, totalDistance);
                }
            }

            passengers[i].UpdatePosition(GetPosition(passengers[i].distanceFromStart));
        }
    }

    public enum MovementType
    {
        PingPong,
        Loop,
        Clamp,
    }
}