﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using ProGrids;

[CustomEditor(typeof(SplineMover))]
public class SplineMoverEditor : SplinePathEditor
{
    [MenuItem("GameObject/HeroTools/SplineMover", false, 10)]
    public static void CreateTool(MenuCommand menuCommand)
    {
        GameObject go = new GameObject("SplineMover");
        SplineMover sm = go.AddComponent<SplineMover>();
        sm.snapValue = pg_Editor.SnapValue();
        sm.Initialize();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Place Objects"))
        {
            SplineMover mover = (SplineMover)target;
            mover.PlaceObjects();
        }
    }
}