﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

public class TextWriter : RadixMonoBehaviour
{
    private const float BASE_LETTER_SCALE = 0.25f;
    private const float SPACE_BETWEEN_LETTERS = 0.4f;
    private const float SPACE_BETWEEN_WORDS = 0.6f;
    private const float SPACE_BETWEEN_LINES = 0.6f;

    [TextArea(3, 10), SerializeField]
    private string m_TextToWrite = "";

    [SerializeField]
    private LineDuo m_LineDuoToUse;

    [SerializeField]
    private float m_DrawDelay;

    [SerializeField]
    private bool m_RequiresTriggerHandler = false;

    [SerializeField]
    private List<GameObject> m_Alphabet = new List<GameObject>();

    private Vector2 mNextLetterPosition = Vector2.zero;

    private List<BorderDrawer> mInstantiatedLetters = new List<BorderDrawer>();

    public bool IsDrawing = false;

    private void Start()
    {
        if(!m_RequiresTriggerHandler)
        {
            Write();
        }
    }
    
    public void Write()
    {
        for(int i = 0; i < m_TextToWrite.Length; i++)
        {
            ChooseLetter(m_TextToWrite[i]);
        }
        StartCoroutine(StartDrawing());
        IsDrawing = true;
    }

    public void StopWriting()
    {
        for(int i = 0; i < mInstantiatedLetters.Count; i++)
        {
            mInstantiatedLetters[i].PauseDrawing();
            for(int j = 0; j < mInstantiatedLetters[i].transform.childCount; j++)
            {
                BorderDrawer childDrawer = mInstantiatedLetters[i].transform.GetChild(j).GetComponent<BorderDrawer>();
                if(childDrawer != null)
                {
                    childDrawer.PauseDrawing();
                }
            }
        }
        IsDrawing = false;
    }

    private IEnumerator StartDrawing()
    {
        yield return new WaitForSeconds(m_DrawDelay);
        for (int i = 0; i < mInstantiatedLetters.Count; i++)
        {
            mInstantiatedLetters[i].StartDrawingAtRandomPoint();
            for(int j = 0; j < mInstantiatedLetters[i].transform.childCount; j++)
            {
                BorderDrawer childDrawer = mInstantiatedLetters[i].transform.GetChild(j).GetComponent<BorderDrawer>();
                if(childDrawer != null)
                {
                    childDrawer.StartDrawingAtRandomPoint();
                }
            }
        }

    }

    private void ChooseLetter(char pLetter)
    {
        string pLetterString = pLetter.ToString();
        pLetterString = pLetterString.ToUpper();
        switch (pLetterString)
        {
            case "A":
                InstantiateLetter(m_Alphabet[0]);
                break;
            case "B":
                InstantiateLetter(m_Alphabet[1]);
                break;
            case "C":
                InstantiateLetter(m_Alphabet[2]);
                break;
            case "D":
                InstantiateLetter(m_Alphabet[3]);
                break;
            case "E":
                InstantiateLetter(m_Alphabet[4]);
                break;
            case "F":
                InstantiateLetter(m_Alphabet[5]);
                break;
            case "G":
                InstantiateLetter(m_Alphabet[6]);
                break;
            case "H":
                InstantiateLetter(m_Alphabet[7]);
                break;
            case "I":
                InstantiateLetter(m_Alphabet[8]);
                break;
            case "J":
                InstantiateLetter(m_Alphabet[9]);
                break;
            case "K":
                InstantiateLetter(m_Alphabet[10]);
                break;
            case "L":
                InstantiateLetter(m_Alphabet[11]);
                break;
            case "M":
                InstantiateLetter(m_Alphabet[12]);
                break;
            case "N":
                InstantiateLetter(m_Alphabet[13]);
                break;
            case "O":
                InstantiateLetter(m_Alphabet[14]);
                break;
            case "P":
                InstantiateLetter(m_Alphabet[15]);
                break;
            case "Q":
                InstantiateLetter(m_Alphabet[16]);
                break;
            case "R":
                InstantiateLetter(m_Alphabet[17]);
                break;
            case "S":
                InstantiateLetter(m_Alphabet[18]);
                break;
            case "T":
                InstantiateLetter(m_Alphabet[19]);
                break;
            case "U":
                InstantiateLetter(m_Alphabet[20]);
                break;
            case "V":
                InstantiateLetter(m_Alphabet[21]);
                break;
            case "W":
                InstantiateLetter(m_Alphabet[22]);
                break;
            case "X":
                InstantiateLetter(m_Alphabet[23]);
                break;
            case "Y":
                InstantiateLetter(m_Alphabet[24]);
                break;
            case "Z":
                InstantiateLetter(m_Alphabet[25]);
                break;
            case "~":
                mNextLetterPosition.y -= SPACE_BETWEEN_LINES;
                mNextLetterPosition.x = 0f;
                break;
            case " ":
                mNextLetterPosition.x += SPACE_BETWEEN_WORDS;
                break;
        }
    }

    private void InstantiateLetter(GameObject pToInstantiate)
    {
        GameObject letterObject = Instantiate(pToInstantiate, this.transform.TransformPoint(mNextLetterPosition), this.transform.rotation, this.transform);
        mNextLetterPosition.x += SPACE_BETWEEN_LETTERS;
        BorderDrawer drawer = letterObject.GetComponent<BorderDrawer>();
        if(drawer!= null && m_LineDuoToUse != null)
        {
            mInstantiatedLetters.Add(drawer);
            if(m_LineDuoToUse != null)
            {
                drawer.SetLineDuoPrefab(m_LineDuoToUse);
                for(int i = 0; i < drawer.transform.childCount; i++)
                {
                    BorderDrawer childDrawer = drawer.transform.GetChild(i).GetComponent<BorderDrawer>();
                    if(childDrawer != null)
                    {
                        childDrawer.SetLineDuoPrefab(m_LineDuoToUse);
                    }
                }
            }
        }
    }
}
