﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

[RequireComponent(typeof(PolygonCollider2D))]
public class PredatorSensor : RadixMonoBehaviour
{
    private const string PREDATOR_TAG_NAME = "Predator";

    [SerializeField]
    private Enemy m_Enemy;

    private PolygonCollider2D mPolygonCollider;


    private bool mSpottedPredator = false;

    protected virtual void Start()
    {
        SetDependencies();
    }

    private void SetDependencies()
    {
        mPolygonCollider = GetComponent<PolygonCollider2D>();
    }

    private void Update()
    {
        SensePredators();
    }

    private void SensePredators()
    {
        ContactFilter2D filter = new ContactFilter2D();
        filter.SetLayerMask(PredatorLayerMask);
        filter.useLayerMask = true;
        Collider2D[] overlappedColliders = new Collider2D[1];
        Physics2D.OverlapCollider(mPolygonCollider, filter, overlappedColliders);
        if (overlappedColliders[0] != null && overlappedColliders[0].GetComponent<Predator>() != null)
        {
            CheckIfPredatorIsVisible(overlappedColliders[0].GetComponent<Predator>());
        }
        else
        {
            mSpottedPredator = false;
        }
    }

    private void CheckIfPredatorIsVisible(Predator pMainCharacter)
    {
        Vector2 direction = pMainCharacter.transform.position - m_Enemy.transform.position;
        float distance = Vector2.Distance(pMainCharacter.transform.position, m_Enemy.transform.position);
        RaycastHit2D[] hitInfo = Physics2D.RaycastAll(m_Enemy.transform.position, direction, distance, SolidObjectLayerMask);
        if (hitInfo.Length > 0)
        {
            mSpottedPredator = false;
        }
        else
        {
            mSpottedPredator = true;
        }
        if (mSpottedPredator)
        {
            DispatchEvent(ECombatEvent.SPOTTED_PREDATOR, pMainCharacter, m_Enemy);
        }
    }

    protected int PredatorLayerMask
    {
        get
        {
            return (1 << LayerMask.NameToLayer("Predator"));
        }
    }

    protected int SolidObjectLayerMask
    {
        get
        {
            return (1 << LayerMask.NameToLayer("SolidObject"));
        }
    }

    public bool SpottedPredator()
    {
        return mSpottedPredator;
    }
}
