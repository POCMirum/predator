﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

[RequireComponent(typeof(SpriteRenderer))]
public class Predator : RadixMonoBehaviour
{
    private const float MOVE_DISTANCE = 0.02f;
    private const float MOVE_SPEED = 3000f;
    private const float RADAR_COOLDOWN = 5f;
    private const int MAX_RADAR_CHARGES = 3;
    public const float TOUCH_SENSE_DELAY = 2f;

    private string EAT_SFX = "FreeSFX/MonsterSnarl";

    [SerializeField]
    private GameObject m_RadarWavePrefab;

    [SerializeField]
    private GameObject m_GrowlPrefab;

    [SerializeField]
    private GameObject m_RadarScanChargeIndicators;

    private SpriteRenderer mSpriteRenderer;

    private Rigidbody2D mRigidbody;

    private int mRadarCharges = 0;

    private List<GameObject> mRadarScanChargeIndicators = new List<GameObject>();

    private PredatorCharacterController2D mCharacterController;

    private bool mIsClingingToWall = false;

    public bool CanTouchSense = true;

    public Timer TouchSenseDelayTimer;

    private Ghost mGhost;

    protected virtual void Start()
    {
        SetDependencies();
    }

    private void SetDependencies()
    {
        mCharacterController = GetComponent<PredatorCharacterController2D>();
        mSpriteRenderer = GetComponent<SpriteRenderer>();
        mRigidbody = GetComponent<Rigidbody2D>();
        mRadarCharges = MAX_RADAR_CHARGES;
        mGhost = GetComponentInChildren<Ghost>();
        TouchSenseDelayTimer = new Timer(TOUCH_SENSE_DELAY, OnTouchSenseDelayEnd, false);
        RegisterEvent(ECombatEvent.KILL_PREDATOR, OnKillPredator);
        if (m_RadarScanChargeIndicators.transform.childCount != MAX_RADAR_CHARGES)
        {
            Debug.LogError("The number of starting radar charges is different from the number of indicators!");
        }
        for(int i = 0; i < m_RadarScanChargeIndicators.transform.childCount; i++)
        {
            mRadarScanChargeIndicators.Add(m_RadarScanChargeIndicators.transform.GetChild(i).gameObject);
        }
    }
    
    protected override void Dispose()
    {
        TouchSenseDelayTimer.Dispose();
    }

    private void Update()
    {
        UpdatePlayerCommands();
    }

    private void UpdatePlayerCommands()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if(mRadarCharges > 0)
            {
                InstantiateRadarCircle();
                RemoveRadarCharge();
            } else
            {
                Instantiate(m_GrowlPrefab, this.transform.position, this.transform.localRotation, this.transform);
                DispatchEvent(EAudioEvent.PLAY_SFX, EAT_SFX);
            }
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            mCharacterController.Jump();
        }
        if (Input.GetKey(KeyCode.A))
        {
            if (!mIsClingingToWall)
            {
                mCharacterController.Run(-1);
            }
        } 
        if (Input.GetKey(KeyCode.D))
		{
            if(!mIsClingingToWall)
            {
                mCharacterController.Run(1);
            }
        } 
        if (Input.GetKey(KeyCode.W))
		{
            if(mIsClingingToWall)
            {
                mCharacterController.Run(1);
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (mIsClingingToWall)
            {
                mCharacterController.Run(-1);
            }
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            mCharacterController.StopJump();
        }
    }

    private void InstantiateRadarCircle()
    {
        Instantiate(m_RadarWavePrefab, this.transform.position, this.transform.localRotation, this.transform.parent);
    }

    private void OnCollisionEnter2D(Collision2D pCollision)
	{
        if (pCollision.collider.tag == Enemy.tag)
        {
            EatEnemy(pCollision.collider.GetComponent<Enemy>());
        } else if (pCollision.collider.tag == "SolidObject")
        {
            if (Mathf.Approximately(pCollision.contacts[0].normal.x,-1f) || Mathf.Approximately(pCollision.contacts[0].normal.x, 1f))
            {
                mCharacterController.SetIsRunningOnWalls(true, Mathf.CeilToInt(-pCollision.contacts[0].normal.x));
            }
        }
    }

    private void OnCollisionStay2D(Collision2D pCollision)
    {
        if (pCollision.collider.tag == "SolidObject" && CanTouchSense)
        {
            Ghost otherGhost = pCollision.gameObject.GetComponentInChildren<Ghost>();
            if(otherGhost != null)
            {
                otherGhost.ForceAvatarDraw(pCollision);
                TouchSenseDelayTimer.Restart();
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "SolidObject")
        {
            mCharacterController.SetIsRunningOnWalls(false, 0);
        }
    }

    private void EatEnemy(Enemy pEnemyToEat)
    {
        DispatchEvent(EAudioEvent.PLAY_SFX, EAT_SFX);
        DispatchEvent(ECombatEvent.KILL_ENEMY, pEnemyToEat);
        RecoverRadarCharges();
    }

    private void RemoveRadarCharge()
    {
        mRadarCharges--;
        mRadarScanChargeIndicators[mRadarCharges].SetActive(false);
    }

    private void RecoverRadarCharges()
    {
        mRadarCharges = MAX_RADAR_CHARGES;
        for(int i = 0; i < mRadarScanChargeIndicators.Count;i++)
        {
            mRadarScanChargeIndicators[i].SetActive(true);
        }
    }

    private void OnKillPredator()
    {
        Debug.Log("GameOver");
    }

    private void OnTouchSenseDelayEnd()
    {
        CanTouchSense = true;
    }
}
