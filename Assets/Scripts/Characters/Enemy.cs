﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Radix;

public class Enemy : RadixMonoBehaviour
{
    public static string tag = "Enemy";
    private float MOVE_DISTANCE = 0.02f;
    private float MOVE_SPEED = 3000f;
    private float TIME_BETWEEN_KILLED_SOUNDS = 0.1f;
    private float TIME_BETWEEN_SNORES = 4.5f;

    private string GASP_SFX = "FreeSFX/Gasp";
    private string GUNSHOT_SFX = "FreeSFX/Gunshot";
    private string DEATH_SFX = "FreeSFX/EnemyDeath";
    private string SNORING_SFX = "FreeSFX/Snoring";

    public static float STARTLED_TIME = 3f;

    [SerializeField]
    private GameObject m_StartledWavePrefab;
    [SerializeField]
    private GameObject m_KilledWavePrefab;
    [SerializeField]
    private GameObject m_GunshotWavePrefab;

    [SerializeField]
    private GameObject m_ExclamationMarkPrefab;
    
    [SerializeField]
    private PredatorSensor m_PredatorSensor;
    [SerializeField]
    private int m_Direction = 1;
    [SerializeField]
    private EPatrolType m_PatrolType = EPatrolType.IDLE;
    [SerializeField]
    private Transform m_LeftPatrolLimit;
    [SerializeField]
    private Transform m_RightPatrolLimit;

    private Rigidbody2D mRigidbody;

    private Timer mStartledTimer;

    private GameObject mExclamationMark;

    private bool mIsStartled = false;

    private CharacterController2D mCharacterController;

    private PolygonCollider2D mCollider;

    private Timer mKilledWavesTimer; //Later, this should be timed with the actual murder SFX;

    private Timer mSnoreTimer;

    private int mNumberOfKilledWavesSent = 0;
    private int mNumberOfKilledWavesToSend = 3;

    protected virtual void Start()
    {
        SetDependencies();
    }

    private void SetDependencies()
    {
        mRigidbody = GetComponent<Rigidbody2D>();
        mCollider = GetComponent<PolygonCollider2D>();
        mStartledTimer = new Timer(STARTLED_TIME, OnStartledTimerEnd);
        mCharacterController = GetComponent<CharacterController2D>();
        mKilledWavesTimer = new Timer(TIME_BETWEEN_KILLED_SOUNDS, OnKilledWavesTimerEnd, true);
        mSnoreTimer = new Timer(TIME_BETWEEN_SNORES, OnSnoreTimerEnd, true);
        mSnoreTimer.Start();
        if(m_Direction == -1)
        {
            Flip();
        }
        RegisterEvent<PredatorEnemyDelegate>(ECombatEvent.SPOTTED_PREDATOR, OnSpottedPredator);
        RegisterEvent<EnemyDelegate>(ECombatEvent.KILL_ENEMY, OnKillEnemy);
    }

    private void Update()
    {
        Patrol();
    }

    private void Patrol()
    {
        if (m_PatrolType == EPatrolType.TURNING)
        {


        } else if (m_PatrolType == EPatrolType.PATROLLING)
        {
            if (m_Direction == -1 && this.transform.position.x < m_LeftPatrolLimit.position.x)
            {
                Flip();
            } else if (m_Direction == 1 && this.transform.position.x > m_RightPatrolLimit.position.x)
            {
                Flip();
            }
            mCharacterController.Run(m_Direction);
        } else if(m_PatrolType == EPatrolType.SLEEPING)
        {
            m_PredatorSensor.gameObject.SetActive(false);
        }
        else
        {
            //Do nothing
        }
    }

    private void OnSnoreTimerEnd()
    {
        if(m_PatrolType == EPatrolType.SLEEPING)
        {
            InstantiateSnoringNoise();
        }
    }

    protected override void Dispose()
    {
        mKilledWavesTimer.Dispose();
        mStartledTimer.Dispose();
        mSnoreTimer.Dispose();
    }

    private void OnCollisionEnter2D(Collision2D pCollision)
    {
        RadarScan noise = pCollision.gameObject.GetComponent<RadarScan>();
        if (noise != null && noise.CanBeHeard())
        {
            Physics2D.IgnoreCollision(pCollision.collider, mCollider);
            BeStartled(noise.GetOrigin());
        }
    }

    private void MoveCharacter(Vector2 pNewPosition)
    {
        mRigidbody.velocity = Vector2.zero;
        float xSpeed = MOVE_SPEED * (pNewPosition.x - this.transform.position.x);
        float ySpeed = MOVE_SPEED * (pNewPosition.y - this.transform.position.y);
        mRigidbody.AddForce(new Vector2(xSpeed, ySpeed));
    }

    private void OnSpottedPredator(Predator pSpottedPredator, Enemy pEnemy)
    {
        if (pEnemy == this)
        {
            BeStartled(pSpottedPredator.transform.position);
        }
    }

    private void BeStartled(Vector2 pStartleOrigin)
    {
        if (!mIsStartled && m_PatrolType != EPatrolType.SLEEPING)
        {
            mIsStartled = true;
            mStartledTimer.Start();
            InstantiateExclamationMark();
            InstantiateStartledNoise();
            StopMoving();
            Investigate(pStartleOrigin);
        }
    }

    private void InstantiateExclamationMark()
    {
        Vector2 instantiationPosition = new Vector2(this.transform.position.x - 0.1f, this.transform.position.y + 1f);
        mExclamationMark = Instantiate(m_ExclamationMarkPrefab, instantiationPosition, this.transform.localRotation, this.transform);
    }


    private void OnStartledTimerEnd()
    {
        Destroy(mExclamationMark);
        if (m_PredatorSensor.SpottedPredator())
        {
            ShootAtTarget();
        }
        else
        {
            mIsStartled = false;
        }
    }

    private void ShootAtTarget()
    {
        if (m_PredatorSensor.SpottedPredator())
        {
            DispatchEvent(ECombatEvent.KILL_PREDATOR);
            InstantiateShotNoise();
        }
    }

    private void InstantiateStartledNoise()
    {
        Instantiate(m_StartledWavePrefab, this.transform.position, this.transform.localRotation, this.transform.parent.parent);
        DispatchEvent(EAudioEvent.PLAY_SFX, GASP_SFX);
    }

    private void InstantiateShotNoise()
    {
        Instantiate(m_GunshotWavePrefab, this.transform.position, this.transform.localRotation, this.transform.parent.parent);
        DispatchEvent(EAudioEvent.PLAY_SFX, GUNSHOT_SFX);
    }

    private void InstantiateSnoringNoise()
    {
        Instantiate(m_StartledWavePrefab, this.transform.position, this.transform.localRotation, this.transform.parent.parent);
        DispatchEvent(EAudioEvent.PLAY_SFX, SNORING_SFX);
    }

    private void OnKillEnemy(Enemy pEnemy)
    {
        if (pEnemy == this)
        {
            DispatchEvent(EAudioEvent.PLAY_SFX, DEATH_SFX);
            Instantiate(m_KilledWavePrefab, this.transform.position, this.transform.localRotation, this.transform.parent.parent);
            mCollider.enabled = false;
            mKilledWavesTimer.Start();
        }
    }

    private void Investigate(Vector2 pStartleOrigin)
    {
        //if noise source is unseen and unreachable
        TurnTowardsNoise(pStartleOrigin);
    }

    private void TurnTowardsNoise(Vector2 pStartleOrigin)
    {
        if(this.transform.position.x < pStartleOrigin.x && m_Direction == -1)
        {
            Flip();
        } else if (this.transform.position.x > pStartleOrigin.x && m_Direction == 1)
        {
            Flip();
        }
    }

    private void Flip()
    {
        Vector3 scale = this.transform.localScale;
        scale *= -1;
        m_Direction = Mathf.RoundToInt(scale.x);
        this.transform.localScale = scale;
    }

    private void StopMoving()
    {
        //mPolyNavAgent.Stop();
    }

    private void OnKilledWavesTimerEnd()
    {
        Instantiate(m_KilledWavePrefab, this.transform.position, this.transform.localRotation, this.transform.parent.parent);
        mNumberOfKilledWavesSent++;
        if(mNumberOfKilledWavesSent >= mNumberOfKilledWavesToSend)
        {
            mKilledWavesTimer.Stop();
            Destroy(this.gameObject.transform.parent.gameObject);
        }
    }
}
