﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Radix;

public class PredatorCharacterController2D : PhysicsObject {
    [SerializeField]
    private float m_MaxSpeed = 4f;
    [SerializeField]
    private float m_MaxClimbingSpeed = 4f;
    [SerializeField]
    private float m_JumpTakeOffSpeed = 2f;
    [SerializeField]
    private float m_JumpHorizontalSpeed = 12f;

    private int mRunDirection = 0;
    private bool mIsJumping = false;
    private bool mHasJumped = false; //Used to tell if the predator left the ground from a jump or from falling
    private bool mIsRunningOnWalls = false;
    private int mLastFacedDirection = 1;
    private int mWallDirection = 1; //1 = right, -1 = left

    protected override void FixedUpdate()
    {
        if(!mIsRunningOnWalls)
        {
            base.FixedUpdate();
        } else
        {
            velocity.y = mTargetVelocity.y;
            mIsGrounded = false;

            Vector2 moveAlongWall = new Vector2(mGroundNormal.x, mGroundNormal.y);
            //Vector2 moveAlongWall = new Vector2(mGroundNormal.y, -mGroundNormal.x);

            Vector2 deltaPosition = velocity * Time.deltaTime;
            
            Vector2 movement = moveAlongWall * deltaPosition.x;

            ApplyMovement(movement, false);

            movement = Vector2.up * deltaPosition.y;

            ApplyMovement(movement, true);
        }
    }

    protected override void ComputeVelocity()
    {
        Vector2 movement = Vector2.zero;
        
        if(!mIsRunningOnWalls)
        {
            movement.x = mRunDirection;
        } else
        {
            movement.y = mRunDirection;
        }
        if(mIsJumping && mIsGrounded)
        {
            //takeoff
            velocity.y = m_JumpTakeOffSpeed;
            mTargetVelocity.x = m_JumpHorizontalSpeed*mLastFacedDirection;
            mHasJumped = true;
        }
        else if (mIsJumping && !mIsGrounded && mIsRunningOnWalls)
        {
            mTargetVelocity.x = m_JumpHorizontalSpeed * -mWallDirection;
            mLastFacedDirection = -mWallDirection;
            SetIsRunningOnWalls(false, 0);
            mHasJumped = true;
        }
        else if(!mIsJumping)
        {
            if(velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f; //reduce y velocity if no longer jumping
            }
        }

        if(mIsRunningOnWalls)
        {
            mHasJumped = false;
            mTargetVelocity = movement * m_MaxClimbingSpeed;
        }
        else if (mIsJumping || (!mIsJumping && mHasJumped && !mIsGrounded))
        {
            //TODO Problem : Predator ends up here when falling without having jumped.
            mTargetVelocity.x = m_JumpHorizontalSpeed * mLastFacedDirection;
        }
        else if (!mIsJumping)
        {
            mHasJumped = false;
            mTargetVelocity = movement * m_MaxSpeed;
        } 


        mRunDirection = 0;
    }

    public void Run(int pRunDirection)
    {
        //1 is right or up, -1 is left or down
        mRunDirection = pRunDirection * mWallDirection;
        mLastFacedDirection = mRunDirection;
    }

    public void Jump()
    {
        mIsJumping = true;
    }

    public void StopJump()
    {
        mIsJumping = false;
    }

    public void SetIsRunningOnWalls(bool pIsRunningOnWalls, int pWallDirection)
    {
        mIsRunningOnWalls = pIsRunningOnWalls;
        if(!mIsRunningOnWalls)
        {
            mWallDirection = 1;
        } else
        {
            mWallDirection = pWallDirection;
        }
    }

}
