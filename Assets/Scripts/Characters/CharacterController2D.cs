﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Radix;

public class CharacterController2D : PhysicsObject {
    [SerializeField]
    private float m_MaxSpeed = 4f;
    [SerializeField]
    private float m_JumpTakeOffSpeed = 7f;

    private int mRunDirection = 0;
    private bool mIsJumping = false;

    protected override void ComputeVelocity()
    {
        Vector2 movement = Vector2.zero;
        movement.x = mRunDirection;
        if (mIsJumping && mIsGrounded)
        {
            velocity.y = m_JumpTakeOffSpeed;
        }
        else if (!mIsJumping)
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }

        mTargetVelocity = movement * m_MaxSpeed;


        mRunDirection = 0;
    }

    public void Run(int pRunDirection)
    {
        mRunDirection = pRunDirection;
    }

    public void Jump()
    {
        mIsJumping = true;
    }

    public void StopJump()
    {
        mIsJumping = false;
    }

}
