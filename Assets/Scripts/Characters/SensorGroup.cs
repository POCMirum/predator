﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Radix;

public class SensorGroup : RadixMonoBehaviour {

    [SerializeField]
    private CharacterController2D m_CharacterToFollow;
	
	// Update is called once per frame
	void Update () {
        this.transform.position = m_CharacterToFollow.transform.position; // This is a shitty fix to make sure the sensor follows the enemy. It is my fix to a unity bug afflicting Rigidbody2d.Cast in the character physics

        Vector3 characterScale = m_CharacterToFollow.transform.localScale;
        this.transform.localScale = characterScale;
    }
}
