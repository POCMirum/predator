﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplinePlacer : SplinePath
{
    [Header("Spline Placer")]
    public GameObject objectToPlace;
    public int amount;

    public void PlaceObjects()
    {
        List<Vector3> spawnpoints = GetPlacementPosition(amount);
        GameObject parent = new GameObject($"Placement_{objectToPlace.name}");
        for (int i = 0; i < spawnpoints.Count; ++i)
        {
            GameObject.Instantiate(objectToPlace, spawnpoints[i], Quaternion.identity, parent.transform);
        }
    }
}