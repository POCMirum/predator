﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using ProGrids;

[CustomEditor(typeof(SplinePlacer))]
public class SplinePlacerEditor : SplinePathEditor
{
    [MenuItem("GameObject/HeroTools/SplinePlacer", false, 10)]
    public static void CreateTool(MenuCommand menuCommand)
    {
        GameObject go = new GameObject("PlacementTool");
        SplinePlacer sp = go.AddComponent<SplinePlacer>();
        sp.snapValue = pg_Editor.SnapValue();
        sp.Initialize();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Place Objects"))
        {
            SplinePlacer placer = (SplinePlacer)target;
            placer.PlaceObjects();
        }
    }
}